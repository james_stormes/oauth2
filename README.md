# Command Line Docker Quick Start:

* Make sure you have Docker and Docker-Compose installed on your workstation.
    * [https://www.docker.com/community-edition](https://www.docker.com/community-edition)
    * Disable any existing web server running on ports 443 and 80
    * Disable any MySQL or MariaDB server running on port 3306
* Start the Docker Container from the project directory:
    * `docker-compose run --service-ports lamp_server bash`
* Install the server Composer Packages:
    * `cd auth`
    * `composer install`
    * `development-enable`
    * `cd ../expressive`
    * `composer install`
    * `development-enable`
    * `cd ..`
    * For development private and public keys are included, you MUST change these for production.

* Open the site on your workstation
    * https://expressive.loopback.world/
* Hardcoded user
    * User Name `test@test.com`
    * Password  `naked`

# Directory layout (AKA Apps)

Each top level directory is served via Apache as a HTTPS endpoint.  
  
Another way to say this is that each top level directory is part of a `loopback.world` domain name.
So `auth` directory is served as [https://auth.loopback.world](https://auth.loopback.world), the `js` 
directory is served as [https://js.loopback.world](https://js.loopback.world), and so on...
    
# Developer Notes

## OAuth2 configuration 

Many of the OAuth2 parameters like Refresh Token life and allowed OAuth Grant types are controlled by the
file `auth\src\OAuth2\src\Handler\OAuth2HandlerFactory.php`.  At some point these will be moved into 
the container space to be configured by the implementor.

##  Logout URL 

https://auth.loopback.world/signout

## Test API Endpoint

There is a test RESTful endpoint in the expressive application at `https://expressive.loopback.world/api/ping`.
This endpoint is protected by OAuth2 and requires a Token or valid session.
    
# Troubleshooting

* This setup requires the latest version of the Docker Images.  You may need to prune your docker images to get this
setup working.
    * `docker system prune -a`
* Docker can also become unstable, you may need to restart your Docker background process.

## Parsing the json formatted log

From inside the Docker bash prompt you can query the apache json formatted logs:

* Formatting a json log
    * `jq --slurp . /var/log/apache2/json.log`
* Tailing a json log
    * `tail -f /var/log/apache2/json.log | jq`
    * `tail -f /var/log/apache2/error.log | jq`
    
See [https://stedolan.github.io/jq/manual/#Invokingjq](https://stedolan.github.io/jq/manual/#Invokingjq) for more 
options.

## Local MariaDB

* PhpMyAdmin [https://sql.loopback.world](https://sql.loopback.world)
    * User is 'root'
    * Password is 'naked'.
   

# Testing

## Integration testing

From the auth directory in the Docker container run:

`./intergration_test`    
   
## Manually testing the client credentials grant example

Send the following cURL request:

```
curl -X "POST" "https://auth.loopback.world/oauth2" \
	-H "Content-Type: application/x-www-form-urlencoded" \
	-H "Accept: 1.0" \
	--data-urlencode "grant_type=client_credentials" \
	--data-urlencode "client_id=myawesomeapp" \
	--data-urlencode "client_secret=abc123" \
	--data-urlencode "scope=basic email"
```

## Manually testing the password grant example

Send the following cURL request:

```
curl -X "POST" "http://auth.loopback.world/oauth2" \
	-H "Content-Type: application/x-www-form-urlencoded" \
	-H "Accept: 1.0" \
	--data-urlencode "grant_type=password" \
	--data-urlencode "client_id=myawesomeapp" \
	--data-urlencode "client_secret=abc123" \
	--data-urlencode "username=test@test.com" \
	--data-urlencode "password=naked" \
	--data-urlencode "scope=basic email"
```

## Manually testing the refresh token grant example

Send the following cURL request. Replace `{{REFRESH_TOKEN}}` with a refresh token from another grant above:

```
curl -X "POST" "http://auth.loopback.world/oauth2" \
	-H "Content-Type: application/x-www-form-urlencoded" \
	-H "Accept: 1.0" \
	--data-urlencode "grant_type=refresh_token" \
	--data-urlencode "client_id=myawesomeapp" \
	--data-urlencode "client_secret=abc123" \
	--data-urlencode "refresh_token={{REFRESH_TOKEN}}"
```

## Manual Authorization Code Grant 

Unit Testing for Authorization Code Grant is currently behind.

Currently to test Authorization Code Grant you can use the sample Zend Expressive Application:

[https://expressive.loopback.world/](https://expressive.loopback.world/)

Use the email address `test@test.com`.

Use the password `naked`.

Old URLs used for temp testing.  Not longer working but here for reference.

`https://oauth2.loopback.world/auth_code.php/authorize?response_type=code&client_id=myawesomeapp&redirect_uri=http://foo/bar`


`https://auth.loopback.world/oauth2/authorization?response_type=code&client_id=myawesomeapp&redirect_uri=http://app1.loopback.world&scope=basic`

## CORS testing

curl -i -X OPTIONS -H "Access-Control-Request-Method: GET"  \
  -H "Access-Control-Request-Headers: Accept, Content-Type"  \
  -H "Origin: http://localhost" -H "Accept: application/json" https://expressive.loopback.world/api/ping

curl -i -X OPTIONS -H "Access-Control-Request-Method: OPTIONS"  \
  -H "Access-Control-Request-Headers: Accept, Content-Type"  \
  -H "Origin: http://localhost" -H "Accept: application/json" https://expressive.loopback.world/api/ping

## Man Implicit Code Grant

TBD


### Clients

OAuth2HtmlMiddleware - for classic HTML applications.
OAuth2JsonMiddleware - For restful servers
OAuth2JavaScriptClient - For Javascript Clients, works in conjugation with OAuth2JsonMiddleware


# OAuth Client Demonstrations ("demo")

## Overview

The demo sub-project includes a Javascript module called ```oauth-implicit```, along with demos using the JQuery, Angular 1.5 and Angular 6 frameworks.  The demos all depend upon the ```oauth-implicit``` module, which can be used as its native TypeScript or imported as a transpiled Javascript module (this demo uses SystemJS, but there are other mechanism, such as RequireJS).

All of the sites demonstrate responding to OAuth state, the ability to login and logout of OAuth, and calling a "ping" test API service.

## typescript install

 npm install -g typescript

## JavaScript Demos Setup

### JQuery

* From the ```jquery/oauth-implicit``` directory, execute `npm install` then ```npm run build```, which will 
  create the Javascript files in the ```dist``` subdirectory.

### NG 1.5

* from the `ng15/public` directory execute `npm install`


### Other 
* From the ```demo/ng1.5``` directory, execute ```npm install```
* From the ```demo``` directory, execute ```docker-compose build``` and then ```docker-compose up```

## Components

### Angular 6

* To launch local, debuggable instance of the Angular 6 site, run ```ng serve``` from the ```ng6``` directory
* To build the demo site, exeucte ```ng build --prod --base-href='ng6'```

### oauth-implicit

* To run the Javascript transpilation, execute ```npm run build```
* To run unit tests, execute ```npm run test```
* To run unit tests coverage, execute ```npm run coverage```