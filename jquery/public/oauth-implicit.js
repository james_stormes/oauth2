"use strict";
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Embedded Base64 encoder in case state gets complicated
 */
exports.Base64 = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = exports.Base64._utf8_encode(e); while (f < e.length) {
        n = e.charCodeAt(f++);
        r = e.charCodeAt(f++);
        i = e.charCodeAt(f++);
        s = n >> 2;
        o = (n & 3) << 4 | r >> 4;
        u = (r & 15) << 2 | i >> 6;
        a = i & 63;
        if (isNaN(r)) {
            u = a = 64;
        }
        else if (isNaN(i)) {
            a = 64;
        }
        t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
    } return t; }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9\+\/\=]/g, ""); while (f < e.length) {
        s = this._keyStr.indexOf(e.charAt(f++));
        o = this._keyStr.indexOf(e.charAt(f++));
        u = this._keyStr.indexOf(e.charAt(f++));
        a = this._keyStr.indexOf(e.charAt(f++));
        n = s << 2 | o >> 4;
        r = (o & 15) << 4 | u >> 2;
        i = (u & 3) << 6 | a;
        t = t + String.fromCharCode(n);
        if (u != 64) {
            t = t + String.fromCharCode(r);
        }
        if (a != 64) {
            t = t + String.fromCharCode(i);
        }
    } t = exports.Base64._utf8_decode(t); return t; }, _utf8_encode: function (e) { e = e.replace(/\r\n/g, "\n"); var t = ""; for (var n = 0; n < e.length; n++) {
        var r = e.charCodeAt(n);
        if (r < 128) {
            t += String.fromCharCode(r);
        }
        else if (r > 127 && r < 2048) {
            t += String.fromCharCode(r >> 6 | 192);
            t += String.fromCharCode(r & 63 | 128);
        }
        else {
            t += String.fromCharCode(r >> 12 | 224);
            t += String.fromCharCode(r >> 6 & 63 | 128);
            t += String.fromCharCode(r & 63 | 128);
        }
    } return t; }, _utf8_decode: function (e) { var t = ""; var n = 0; var c1 = 0, c2 = 0, c3 = 0, r = 0; while (n < e.length) {
        r = e.charCodeAt(n);
        if (r < 128) {
            t += String.fromCharCode(r);
            n++;
        }
        else if (r > 191 && r < 224) {
            c2 = e.charCodeAt(n + 1);
            t += String.fromCharCode((r & 31) << 6 | c2 & 63);
            n += 2;
        }
        else {
            c2 = e.charCodeAt(n + 1);
            c3 = e.charCodeAt(n + 2);
            t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
            n += 3;
        }
    } return t; } };
/**
 * Event type enumeration
 */
var OAuthImplicitEventType;
(function (OAuthImplicitEventType) {
    OAuthImplicitEventType[OAuthImplicitEventType["Initialized"] = 0] = "Initialized";
    OAuthImplicitEventType[OAuthImplicitEventType["TokenGranted"] = 1] = "TokenGranted";
    OAuthImplicitEventType[OAuthImplicitEventType["TokenRetrieved"] = 2] = "TokenRetrieved";
    OAuthImplicitEventType[OAuthImplicitEventType["TokenRenewed"] = 3] = "TokenRenewed";
    OAuthImplicitEventType[OAuthImplicitEventType["TokenExpired"] = 4] = "TokenExpired";
    OAuthImplicitEventType[OAuthImplicitEventType["Logout"] = 5] = "Logout";
    OAuthImplicitEventType[OAuthImplicitEventType["Error"] = 6] = "Error";
})(OAuthImplicitEventType = exports.OAuthImplicitEventType || (exports.OAuthImplicitEventType = {}));
;
/**
 * Authentication status
 */
var OAuthStatus;
(function (OAuthStatus) {
    OAuthStatus[OAuthStatus["Unknown"] = 0] = "Unknown";
    OAuthStatus[OAuthStatus["Authenticated"] = 1] = "Authenticated";
    OAuthStatus[OAuthStatus["NotAuthenticated"] = 2] = "NotAuthenticated";
})(OAuthStatus = exports.OAuthStatus || (exports.OAuthStatus = {}));
/**
 * Information to send back on event
 */
var OAuthStatusInformation = /** @class */ (function () {
    function OAuthStatusInformation(status, accessToken, accessTokenExpiration, state) {
        this.status = status;
        this.accessToken = accessToken;
        this.accessTokenExpiration = accessTokenExpiration;
        this.state = state;
    }
    return OAuthStatusInformation;
}());
exports.OAuthStatusInformation = OAuthStatusInformation;
/**
 * Configuration for OAuth Implicit workflow
 */
var OAuthImplicitConfiguration = /** @class */ (function () {
    function OAuthImplicitConfiguration(clientId, window, tokenStorage, tokenStorageKey, requestStateStorage, requestStateStorageKey) {
        if (tokenStorage === void 0) { tokenStorage = window.localStorage; }
        if (tokenStorageKey === void 0) { tokenStorageKey = undefined; }
        if (requestStateStorage === void 0) { requestStateStorage = window.sessionStorage; }
        if (requestStateStorageKey === void 0) { requestStateStorageKey = undefined; }
        this.silentRefreshIFrameName = 'OAUTH_IFRAME';
        this.displayRefreshIFrame = false;
        this._clientId = clientId;
        this._window = window;
        this._tokenStorage = tokenStorage;
        this._requestStateStorage = requestStateStorage;
        this._tokenStorageKey = tokenStorageKey ? tokenStorageKey : 'oauth-token-' + clientId;
        this._requestStateStorageKey = requestStateStorageKey ? requestStateStorageKey : 'oauth-request-' + clientId;
    }
    Object.defineProperty(OAuthImplicitConfiguration.prototype, "clientId", {
        get: function () {
            return this._clientId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OAuthImplicitConfiguration.prototype, "window", {
        get: function () {
            return this._window;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OAuthImplicitConfiguration.prototype, "tokenStorage", {
        get: function () {
            return this._tokenStorage;
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(OAuthImplicitConfiguration.prototype, "requestStateStorage", {
        get: function () {
            return this._requestStateStorage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OAuthImplicitConfiguration.prototype, "tokenStorageKey", {
        get: function () {
            return this._tokenStorageKey;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OAuthImplicitConfiguration.prototype, "requestStateStorageKey", {
        get: function () {
            return this._requestStateStorageKey;
        },
        enumerable: true,
        configurable: true
    });
    return OAuthImplicitConfiguration;
}());
exports.OAuthImplicitConfiguration = OAuthImplicitConfiguration;
/**
 * OAuth Implicit client handler
 */
var OAuthImplicit = /** @class */ (function () {
    function OAuthImplicit(configuration) {
        var _this = this;
        this._subscriptions = [];
        this._status = OAuthStatus.Unknown;
        this._accessToken = undefined;
        this._accessTokenExpiration = undefined;
        /**
         * Dispatch notifications to subscribed callbacks
         */
        this.dispatchNotification = function (event, payload) {
            var e_1, _a;
            try {
                for (var _b = __values(_this._subscriptions), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var subscription = _c.value;
                    subscription(event, payload);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        };
        this._configuration = configuration;
    }
    Object.defineProperty(OAuthImplicit.prototype, "status", {
        /**
         * Return whether a valid access token exists
         */
        get: function () {
            return this._status;
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(OAuthImplicit.prototype, "accessToken", {
        /**
         * Return the current access token
         */
        get: function () {
            return this._accessToken;
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(OAuthImplicit.prototype, "accessTokenExpiration", {
        /**
         * Return the current access token expiration
         */
        get: function () {
            return this._accessTokenExpiration;
        },
        enumerable: true,
        configurable: true
    });
    ;
    /**
     * Initialize, optionally using a specific hash
     * @param hash
     */
    OAuthImplicit.prototype.initialize = function (hash) {
        if (hash === void 0) { hash = undefined; }
        // Check to see if we got an authentication response
        this.checkForAuthorizationResponse(hash);
        var status = this._status;
        if (status !== OAuthStatus.Authenticated) {
            // If this isn't a redirect with an authorization response, check storage for token
            this.checkStorageForToken();
        }
        status = this._status;
        if (status !== OAuthStatus.Authenticated) {
            // If still not authenticated, then dispatch a notification letting the client know we tried but could not authenticate
            this._status = OAuthStatus.NotAuthenticated;
            this.dispatchNotification(OAuthImplicitEventType.Initialized, new OAuthStatusInformation(OAuthStatus.NotAuthenticated));
        }
    };
    /**
     * Process a response URL, looking for access token information
     */
    OAuthImplicit.prototype.checkForAuthorizationResponse = function (hash) {
        if (hash === void 0) { hash = undefined; }
        var e_2, _a;
        if (!hash) {
            hash = this._configuration.window.location.hash;
        }
        hash = decodeURIComponent(hash);
        if (hash.indexOf('#') !== 0) {
            // If there is no hash, exit out
            return;
        }
        var questionMarkPosition = hash.indexOf('?');
        if (questionMarkPosition > -1) {
            hash = hash.substr(questionMarkPosition + 1);
        }
        else {
            hash = hash.substr(1);
        }
        var accessToken = undefined;
        var accessTokenExpiresIn = undefined;
        var state = undefined;
        var parts = hash.split('&');
        try {
            for (var parts_1 = __values(parts), parts_1_1 = parts_1.next(); !parts_1_1.done; parts_1_1 = parts_1.next()) {
                var part = parts_1_1.value;
                var equalPosition = part.indexOf('=');
                if (equalPosition === -1) {
                    continue;
                }
                var name_1 = part.substr(0, equalPosition);
                var value = part.substr(equalPosition + 1);
                switch (name_1) {
                    case 'access_token':
                        accessToken = value;
                        break;
                    case 'expires_in': // seconds
                        var exp = parseInt(value);
                        if (exp) {
                            accessTokenExpiresIn = exp;
                        }
                        else {
                            this._status = OAuthStatus.NotAuthenticated;
                            this.removeTokenStorage();
                            this.dispatchNotification(OAuthImplicitEventType.Error, new Error("Invalid access token expiration value: \"" + value + "\""));
                            return;
                        }
                        break;
                    case 'state':
                        state = exports.Base64.decode(value);
                        break;
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (parts_1_1 && !parts_1_1.done && (_a = parts_1.return)) _a.call(parts_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        // If there is an access token after the hash mark, process
        if (accessToken) {
            this.processInboundToken(accessToken, accessTokenExpiresIn, state);
        }
    };
    ;
    /**
     * Check storage for access token and set as valid if not expired
     */
    OAuthImplicit.prototype.checkStorageForToken = function () {
        try {
            var json = this._configuration.tokenStorage.getItem(this._configuration.tokenStorageKey);
            if (!json) {
                return;
            }
            var data = JSON.parse(json);
            if (!data.accessToken) {
                this.dispatchNotification(OAuthImplicitEventType.Error, new Error("accessToken not located in local storage"));
                return;
            }
            if (data.accessTokenExpiration && (Date.now() >= data.accessTokenExpiration)) {
                this.handleExpiredToken(data.accessToken, data.accessTokenExpiration);
            }
            else {
                this._status = OAuthStatus.Authenticated;
                this._accessToken = data.accessToken;
                this._accessTokenExpiration = data.accessTokenExpiration;
                this.setupSilentRefreshIFrame();
                this.dispatchNotification(OAuthImplicitEventType.TokenRetrieved, new OAuthStatusInformation(OAuthStatus.Authenticated, data.accessToken, data.accessTokenExpiration));
            }
        }
        catch (e) {
            this.dispatchNotification(OAuthImplicitEventType.Error, e);
        }
    };
    /**
     * Generate a URL for authentication, generating a nonce to reduce risk of replay attack,
     * and sending state (if provided) to the server so we can get back to where we were
     */
    OAuthImplicit.prototype.generateAuthenticationUrl = function (redirectUri, state, prompt) {
        if (state === void 0) { state = undefined; }
        if (prompt === void 0) { prompt = true; }
        var nonce = this.generateNonce();
        var stateToSend = nonce;
        if (state) {
            stateToSend += '|' + state;
        }
        ;
        // Store state in session storage so we can validate later
        this._configuration.requestStateStorage.setItem(this._configuration.requestStateStorageKey, stateToSend);
        // Generate URL
        var url = this._configuration.authenticationUri +
            '?client_id=' + this._configuration.clientId +
            '&redirect_uri=' + encodeURIComponent(redirectUri) +
            (this._configuration.scopes ? ('&scopes=' + encodeURIComponent(this._configuration.scopes)) : '') +
            '&state=' + exports.Base64.encode(stateToSend) +
            '&response_type=token';
        if (!prompt) {
            url += "&prompt=none";
        }
        return url;
    };
    /**
     * Request a token from the OAuth server, by default, storing the current URL as state
     */
    OAuthImplicit.prototype.requestToken = function (state) {
        if (state === void 0) { state = undefined; }
        if (!state) {
            state = this._configuration.window.location.href;
        }
        this._configuration.window.location.href = this.generateAuthenticationUrl(this._configuration.redirectUri, state);
    };
    ;
    /**
     * Logout
     */
    OAuthImplicit.prototype.logout = function () {
        this._status = OAuthStatus.NotAuthenticated;
        this._accessToken = undefined;
        this._accessTokenExpiration = undefined;
        this.dispatchNotification(OAuthImplicitEventType.Logout, new OAuthStatusInformation(OAuthStatus.NotAuthenticated));
        this.removeTokenStorage();
        return this._configuration.logoutUri;
    };
    /**
     * Subscribe callback to notifications
     * @param {*} callback
     */
    OAuthImplicit.prototype.subscribe = function (callback) {
        var e_3, _a;
        var add = true;
        try {
            for (var _b = __values(this._subscriptions), _c = _b.next(); !_c.done; _c = _b.next()) {
                var subscription = _c.value;
                if (subscription === callback) {
                    add = false;
                    break;
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_3) throw e_3.error; }
        }
        if (add) {
            this._subscriptions.push(callback);
        }
    };
    /**
     * Unsubscribe callback from notifications
     * @param {*} callback
     */
    OAuthImplicit.prototype.unsubscribe = function (callback) {
        var add = false;
        this._subscriptions = this._subscriptions.filter(function (subscription) {
            return subscription !== callback;
        });
    };
    ;
    /**
     * Generate random-ish value
     */
    OAuthImplicit.prototype.generateNonce = function () {
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var text = '';
        for (var i = 0; i < 40; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };
    /**
     * Persists the token
     * @param accessToken
     * @param accessTokenExpiresInSeconds
     * @param state
     */
    OAuthImplicit.prototype.processInboundToken = function (accessToken, accessTokenExpiresInSeconds, state) {
        // Authenticate the state
        try {
            var storedState = this._configuration.requestStateStorage.getItem(this._configuration.requestStateStorageKey);
            if (!storedState) {
                throw new Error("Request state not found in storage");
            }
            if (storedState !== state) {
                throw new Error("Invalid authorization state received");
            }
        }
        catch (e) {
            this._status = OAuthStatus.NotAuthenticated;
            this.removeTokenStorage();
            this.dispatchNotification(OAuthImplicitEventType.Error, e);
            return;
        }
        if (accessTokenExpiresInSeconds < 1) {
            this.handleExpiredToken(accessToken, Date.now() + this.accessTokenExpiration);
        }
        else {
            this._status = OAuthStatus.Authenticated;
            this._accessToken = accessToken;
            this._accessTokenExpiration = Date.now() + accessTokenExpiresInSeconds * 1000;
            // Separate the state (if provided) from nonce
            var pipe = state.indexOf('|');
            var stateWithoutNonce = (pipe === -1) ? undefined : state.substr(pipe + 1);
            this.setupSilentRefreshIFrame();
            this.dispatchNotification(OAuthImplicitEventType.TokenGranted, new OAuthStatusInformation(this._status, this._accessToken, this._accessTokenExpiration, stateWithoutNonce));
            try {
                this._configuration.tokenStorage.setItem(this._configuration.tokenStorageKey, JSON.stringify({
                    accessToken: this._accessToken,
                    accessTokenExpiration: this._accessTokenExpiration,
                    state: stateWithoutNonce
                }));
            }
            catch (e) {
                this.dispatchNotification(OAuthImplicitEventType.Error, e);
            }
        }
    };
    /**
     * Clear token values and let subscribers know the notification expired,
     * this should get called whenever we determine that a token has expired
     */
    OAuthImplicit.prototype.handleExpiredToken = function (accessToken, accessTokenExpiration) {
        this._accessToken = undefined;
        this._accessTokenExpiration = undefined;
        this.dispatchNotification(OAuthImplicitEventType.TokenExpired, new OAuthStatusInformation(OAuthStatus.NotAuthenticated));
        this.removeTokenStorage();
    };
    /**
     * Remove token stored (if any)
     */
    OAuthImplicit.prototype.removeTokenStorage = function () {
        try {
            this._configuration.tokenStorage.removeItem(this._configuration.tokenStorageKey);
        }
        catch (e) {
            this.dispatchNotification(OAuthImplicitEventType.Error, e);
        }
    };
    /**
     * Create a silent refresh iFrame before the expiration, if there is an expiration and configuration for silent iFrame refresh
     */
    OAuthImplicit.prototype.setupSilentRefreshIFrame = function () {
        if (!(this._accessTokenExpiration && this._configuration.silentRedirectUri && this._configuration.silentRefreshIFrameName && this._configuration.window.document)) {
            return;
        }
        var timeout = Math.max((this._accessTokenExpiration - Date.now()) * 0.75, 10000);
        var me = this;
        setTimeout(function () {
            me.insertSilentRefreshIFrame();
        }, timeout);
        // console.info("Refresh login set for " + timeout.toString() + " ms");
    };
    /**
     * Inserts a silent refresh iframe into the configured window's document
     */
    OAuthImplicit.prototype.insertSilentRefreshIFrame = function () {
        var _this = this;
        var document = this._configuration.window.document;
        var existingIframe = document.getElementById(this._configuration.silentRefreshIFrameName);
        if (existingIframe) {
            document.body.removeChild(existingIframe);
        }
        // this.silentRefreshSubject = claims['sub'];
        var iframe = document.createElement('iframe');
        iframe.id = this._configuration.silentRefreshIFrameName;
        var url = this.generateAuthenticationUrl(this._configuration.silentRedirectUri, undefined, false);
        iframe.setAttribute('src', url);
        iframe.style['display'] = this._configuration.displayRefreshIFrame ? 'block' : 'none';
        iframe.style['width'] = this._configuration.displayRefreshIFrame ? '400' : '1';
        iframe.style['height'] = this._configuration.displayRefreshIFrame ? '400' : '1';
        iframe.onload = function (e) {
            // console.log("iframe loaded", e);
            if (e.target && e.target.contentDocument && e.target.contentDocument.URL) {
                var url_1 = e.target.contentDocument.URL;
                var i = url_1.indexOf('#');
                if (i !== -1) {
                    _this.checkForAuthorizationResponse(url_1.substr(i));
                }
            }
        };
        iframe.onerror = function (e) {
            console.error("iframe error", e);
        };
        document.body.appendChild(iframe);
    };
    return OAuthImplicit;
}());
exports.OAuthImplicit = OAuthImplicit;
;
//# sourceMappingURL=oauth-implicit.js.map