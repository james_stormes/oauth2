var exports = {};
var oauth = undefined;
function launch() {
    SystemJS.import("oauth-implicit.js").then((OAuthNamespace) => {
        var config = new OAuthNamespace.OAuthImplicitConfiguration("demojq", window);
        config.authenticationUri = "https://auth.loopback.world/oauth2/authorization";
        config.redirectUri = urlWithoutHash(window.location.href);
        config.silentRedirectUri = urlWithoutHash(window.location.href, 'refresh.html');
        config.silentRefreshIFrameName = 'silent-refresh';
        oauth = new OAuthNamespace.OAuthImplicit(config);
        oauth.subscribe((type, data) => {
            console.log("Received OAuth Notification: " + type.toString());
            console.log(data);
            if(type === OAuthNamespace.OAuthImplicitEventType.Error) {
                // On error, switch UI back to not auth state
                window.alert(data.message);
                console.error(type, data);
                $(".authenticated").toggle(false);
                $(".not-authenticated").toggle(true);
            } else {
                const showAuthenticated = (data.status === OAuthNamespace.OAuthStatus.Authenticated);
                const showNotAuthenticated = (data.status === OAuthNamespace.OAuthStatus.NotAuthenticated);
                console.log(type, data);
                $(".authenticated").toggle(showAuthenticated);
                $(".not-authenticated").toggle(showNotAuthenticated);
                if(type === OAuthNamespace.OAuthImplicitEventType.TokenGranted) {
                    // If the token was granted, clear out any hash and redirect to the state, which was our original URL
                    history.pushState('', document.title, window.location.pathname);
                    if(data.state) {
                        if(window.location.href != data.state) {
                            window.location.href = data.state;
                        }
                    }
                }

            }
        });
        oauth.initialize();
    });
}
function urlWithoutHash(url, secondPart) {
    var i = url.indexOf('#');
    if(i !== -1) {
        url = url.substr(0, i);
    }
    if(secondPart) {
        if(url[url.length - 1] !== '/') {
            url += '/';
        }
        url += secondPart;
    }
    return url;
}
function login() {
    oauth.requestToken();
}
function logout() {
    var logoutUri = oauth.logout();
    if(logoutUri) {
        window.location.href = logoutUri;
    }
}
function back() {
    window.location.href = '/';
}
function ping() {
    const options = {
        url: "https://expressive.loopback.world/api/ping",
        dataType: "json",
        headers: {
            Accept: "application/json",
            Authorization: "Bearer " + oauth.accessToken
        }
    };
    console.log(options);
    $.ajax(options)
    .done(function(data) {
        window.alert("ACK: " + data.ack);
    })
    .fail(function(xhr, status) {
        window.alert("Failure: " + status);
    });
}