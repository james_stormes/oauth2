import { OAuthImplicitEventType, OAuthImplicitConfiguration, OAuthImplicit, Base64, OAuthStatus, OAuthStatusInformation } from '../src/oauth-implicit';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { JSDOM } from 'jsdom';
import MockStorage from './mock-storage';
import * as assert from 'assert';
import * as sinon from 'sinon';
import { createHash } from 'crypto';

describe("OAuthImplicitConfiguration", () => {
    let stubWindow: any;
    beforeEach(() => {
        stubWindow = {
            location: {
                href: undefined,
                hash: undefined
            },
            localStorage: new MockStorage(),
            sessionStorage: new MockStorage()
        };
    })
    it("constructs with defaults", () => {
        const config = new OAuthImplicitConfiguration("123", stubWindow);
        assert.equal(config.clientId, "123");
        assert.equal(config.window, stubWindow);
        assert.equal(config.tokenStorage, stubWindow.localStorage);
        assert.equal(config.requestStateStorage, stubWindow.sessionStorage);
        assert.equal(config.tokenStorageKey, 'oauth-token-123');
        assert.equal(config.requestStateStorageKey, 'oauth-request-123');
    });

    it("constructs with overrides", () => {
        const bogusLocalStorage = new MockStorage();
        const bogusSessionStorage = new MockStorage();
        const config = new OAuthImplicitConfiguration("123", stubWindow, 
            <Storage> bogusLocalStorage, 'Foo', <Storage> bogusSessionStorage, 'Bar');
        assert.equal(config.clientId, "123");
        assert.equal(config.window, stubWindow);
        assert.equal(config.tokenStorage, bogusLocalStorage);
        assert.equal(config.tokenStorageKey, 'Foo');
        assert.equal(config.requestStateStorage, bogusSessionStorage);
        assert.equal(config.requestStateStorageKey, 'Bar');
    });
})

describe("Base64", () => {
    
    let plainText = 'abcdeghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890!@#$%^&*()-=_+";[]{}|\\/?<>,.ҙڟהּ∑рﻼנּ';
    let encodeText = 'YWJjZGVnaGlqa2xtbm9wcXJzdHV2d3h5ekFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaMDEyMzQ1Njc4OTAhQCMkJV4mKigpLT1fKyI7W117fXxcLz88Piwu0pnan++stOKIkdGA77u8762A';

    it("should encode", () => {
        assert.equal(Base64.encode(plainText), encodeText);
    });
    it("should decode", () => {
        assert.equal(Base64.decode(encodeText), plainText);
    });
});

describe("OAuthImplicit", () => {
    const DEFAULT_ACCESS_TOKEN = "xxx-ttt";
    const DEFAULT_EXPIRES_IN = 60001;
    const DEFAULT_REQUEST_STATE = 'customer/12345';
    const DEFAULT_REQUEST_STATE_WITH_NONCE = '12345|' + DEFAULT_REQUEST_STATE;
    const DEFAULT_ENCODED_REQUEST_STATE = Base64.encode(encodeURIComponent(DEFAULT_REQUEST_STATE_WITH_NONCE));
    const DEFAULT_AUTHORIZATION_URI = "https://my.auth.com";
    const DEFAULT_LOGOUT_URI = "https://my.auth.com/logout";
    const DEFAULT_REDIRECT_URI = "http://localhost";
    const DEFAULT_IFRAME_NAME = "MY-OAUTH-IFRAME";
    const DEFAULT_SILENT_REFRESH_URI = "http://localhost/silent.html";
    
    let stubWindow: any;
    let sandbox: sinon.SinonSandbox = undefined;
    let config: OAuthImplicitConfiguration;
    let oauth: OAuthImplicit;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        stubWindow = {
            location: {
                href: undefined,
                hash: undefined
            },
            localStorage: new MockStorage(),
            sessionStorage: new MockStorage()
        };
    
        config = new OAuthImplicitConfiguration("TEST-CLIENT", stubWindow);
        config.authenticationUri = DEFAULT_AUTHORIZATION_URI;
        config.redirectUri = DEFAULT_REDIRECT_URI;
        config.logoutUri = DEFAULT_LOGOUT_URI;
        config.silentRefreshIFrameName = DEFAULT_IFRAME_NAME;
        config.silentRedirectUri = DEFAULT_SILENT_REFRESH_URI;
        oauth = new OAuthImplicit(config);
    });
    
    afterEach(() => {
        sandbox.restore();
    });

    describe("consructor", () => {
        it("returns with configuration assigned", () => {
            assert.equal((<any> oauth)._configuration, config);
        });
    })

    describe("accessToken", () => {
        it("returns accessToken", () => {
            (<any>oauth)._accessToken = DEFAULT_ACCESS_TOKEN;
            assert.equal(oauth.accessToken, DEFAULT_ACCESS_TOKEN);
        })
    });

    describe("accessTokenExpiraton", () => {
        it("returns accessTokenExpiration", () => {
            (<any>oauth)._accessTokenExpiration = 999;
            assert.equal(oauth.accessTokenExpiration, 999);
        })
    });

    describe("initialize", () => {
        let stubCheckAuthResponse: sinon.SinonStub;
        let stubCheckStorage: sinon.SinonStub;
        let stubDispatchNotification: sinon.SinonStub;
        beforeEach(() => {
            stubCheckAuthResponse = sandbox.stub(oauth, "checkForAuthorizationResponse");
            stubCheckStorage = sandbox.stub(oauth, "checkStorageForToken")
            stubDispatchNotification = sandbox.stub(oauth, "dispatchNotification");
        });
        it("passes hash to checkForAuthorizationResponse", () => {
            stubCheckStorage.callsFake(() => {
                (<any>oauth)._status = OAuthStatus.Authenticated;
            });
            oauth.initialize("Nee!");
            assert(stubCheckAuthResponse.withArgs("Nee!").calledOnce);
            assert(stubCheckStorage.calledOnce);
            assert(stubDispatchNotification.notCalled);
        });
        it("should stop if there is an authorization response", () => {
            stubCheckAuthResponse.callsFake(() => {
                (<any>oauth)._status = OAuthStatus.Authenticated;
            });
            oauth.initialize();
            assert(stubCheckAuthResponse.calledOnce);
            assert(stubCheckStorage.notCalled);
            assert(stubDispatchNotification.notCalled);
        });
        it("should stop if there is a stored token", () => {
            stubCheckStorage.callsFake(() => {
                (<any>oauth)._status = OAuthStatus.Authenticated;
            });
            oauth.initialize();
            assert(stubCheckAuthResponse.calledOnce);
            assert(stubCheckStorage.calledOnce);
            assert(stubDispatchNotification.notCalled);
        });
        it("should set NotAuthenticated and dispatch initialized notification if there is no response or stored token", () => {
            oauth.initialize();
            assert(stubCheckAuthResponse.calledOnce);
            assert(stubCheckStorage.calledOnce);
            assert.equal(oauth.status, OAuthStatus.NotAuthenticated);
            assert(stubDispatchNotification.withArgs(OAuthImplicitEventType.Initialized, new OAuthStatusInformation(OAuthStatus.NotAuthenticated)).calledOnce);
        });
    })

    describe("requestToken", () => {
        it("sets window.location.href using specified state", () => {
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .calledWith("Nee!");
            oauth.requestToken("Nee!");
            assert.equal(config.window.location.href, "http://foo");
        });
        it("sets window.location.href with default state of current page location", () => {
            stubWindow.location.href = "http://nee";
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .calledWith("http://nee");
            oauth.requestToken();
            assert.equal(config.window.location.href, "http://foo");
        });
    });

    describe("generateAuthenticationUrl", () => {
        it("encodes with state, generates nonce and stores in session storage", () => {
            sandbox.stub(oauth, "generateNonce")
                .returns("ABCDEF");
            const url = oauth.generateAuthenticationUrl(config.redirectUri, "Nee!");
            assert.equal(url, config.authenticationUri +
                '?client_id=' + config.clientId + 
                '&redirect_uri=' + encodeURIComponent(config.redirectUri) +
                '&state=' + Base64.encode('ABCDEF|Nee!') +
                '&response_type=token');
            assert.deepEqual(config.requestStateStorage.getItem(config.requestStateStorageKey), 'ABCDEF|Nee!');
        });

        it("encodes without state, generates nonce and stores in session storage", () => {
            sandbox.stub(oauth, "generateNonce")
                .returns("YYYZZZ");
            const url = oauth.generateAuthenticationUrl(config.redirectUri);
            assert.equal(url, config.authenticationUri +
                '?client_id=' + config.clientId + 
                '&redirect_uri=' + encodeURIComponent(config.redirectUri) +
                '&state=' + Base64.encode('YYYZZZ') +
                '&response_type=token');
            assert.deepEqual(config.requestStateStorage.getItem(config.requestStateStorageKey), 'YYYZZZ');
        });

        it("encodes with state, generates nonce, no prompt and stores in session storage", () => {
            sandbox.stub(oauth, "generateNonce")
                .returns("ABCDEF");
            const url = oauth.generateAuthenticationUrl(config.redirectUri, "Nee!", false);
            assert.equal(url, config.authenticationUri +
                '?client_id=' + config.clientId + 
                '&redirect_uri=' + encodeURIComponent(config.redirectUri) +
                '&state=' + Base64.encode('ABCDEF|Nee!') +
                '&response_type=token' +
                '&prompt=none');
            assert.deepEqual(config.requestStateStorage.getItem(config.requestStateStorageKey), 'ABCDEF|Nee!');
        });

        it("encodes with state and sopes, generates nonce, no prompt and stores in session storage", () => {
            sandbox.stub(oauth, "generateNonce")
                .returns("ABCDEF");
            config.scopes = 'openid';
            const url = oauth.generateAuthenticationUrl(config.redirectUri, "Nee!", false);
            assert.equal(url, config.authenticationUri +
                '?client_id=' + config.clientId + 
                '&redirect_uri=' + encodeURIComponent(config.redirectUri) +
                '&scopes=' + encodeURIComponent(config.scopes) +
                '&state=' + Base64.encode('ABCDEF|Nee!') +
                '&response_type=token' +
                '&prompt=none');
            assert.deepEqual(config.requestStateStorage.getItem(config.requestStateStorageKey), 'ABCDEF|Nee!');
        });

    });

    describe("checkStorageForToken and handleExpiredToken", () => {
        it("succeeds when there is no key in storage", () => {
            sandbox.stub(config.tokenStorage, "getItem").withArgs(config.tokenStorageKey).returns(null);
            oauth.checkStorageForToken();
            assert.notEqual(oauth.status, OAuthStatus.Authenticated);
        });
        it("dispatches TokenRetrieved when there is a valid key in storage", (done) => {
            const expiration = Date.now() + 999999999;
            config.tokenStorage.setItem(config.tokenStorageKey, JSON.stringify({
                accessToken: DEFAULT_ACCESS_TOKEN,
                accessTokenExpiration: expiration
            }));
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.TokenRetrieved);
                assert.equal(data.accessToken, DEFAULT_ACCESS_TOKEN);
                assert.equal(data.accessTokenExpiration, expiration);
                done();
            });
            oauth.checkStorageForToken();
            assert.equal(oauth.status, OAuthStatus.NotAuthenticated);
            assert.equal(oauth.accessToken, DEFAULT_ACCESS_TOKEN);
            assert.equal(oauth.accessTokenExpiration, expiration);
        });
        it("dispatches TokenExpired when there is an expired key in storage", (done) => {
            const expiration = Date.now() - 999999999;
            config.tokenStorage.setItem(config.tokenStorageKey, JSON.stringify({
                accessToken: DEFAULT_ACCESS_TOKEN,
                accessTokenExpiration: expiration
            }));
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.TokenExpired);
                assert.equal(data.accessToken, undefined);
                assert.equal(data.accessTokenExpiration, undefined);
                done();
            });
            oauth.checkStorageForToken();
            assert.equal(oauth.accessToken, DEFAULT_ACCESS_TOKEN);
            assert.equal(oauth.accessTokenExpiration, expiration);
        });
        it("dispatches TokenExpired when there is an expired key in storage and dispatches error if can't be removed", (done) => {
            const expiration = Date.now() - 999999999;
            let count = 0;
            config.tokenStorage.setItem(config.tokenStorageKey, JSON.stringify({
                accessToken: DEFAULT_ACCESS_TOKEN,
                accessTokenExpiration: expiration
            }));
            sandbox.stub(stubWindow.localStorage, "removeItem")
                .throws(new Error("Unable to remove item from storage"));
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                if(type === OAuthImplicitEventType.TokenExpired) {
                    assert.equal(data.accessToken, undefined);
                    assert.equal(data.accessTokenExpiration, undefined);
                    count++;
                } else if (type === OAuthImplicitEventType.Error) {
                    assert.equal(data.message, "Unable to remove item from storage");
                    count++;
                }
                if(count === 2) {
                    done();
                }
            });
            oauth.checkStorageForToken();
            assert.equal(oauth.accessToken, undefined);
            assert.equal(oauth.accessTokenExpiration, undefined);
        });
        it("dispatches Error when there is is no access token stored", (done) => {
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.Error);
                assert.equal(data.message, "accessToken not located in local storage");
                done();
            });
            config.tokenStorage.setItem(config.tokenStorageKey, '{}');
            oauth.checkStorageForToken();
        });
        it("dispatches Error when there is invalid data stored", (done) => {
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.Error);
                assert.equal(data.message, "Unexpected token } in JSON at position 0");
                done();
            });
            config.tokenStorage.setItem(config.tokenStorageKey, '}');
            oauth.checkStorageForToken();
        });
    });

    describe("checkForAuthorizationResponse", () => {
        it("returns without doing anything if there is no hash", () => {
            oauth.checkForAuthorizationResponse();
            assert.equal(oauth.status, OAuthStatus.Unknown);
        });
        it("returns without doing anything if there is an invalid hash", () => {
            config.window.location.hash = `#access_token${DEFAULT_ACCESS_TOKEN}&expires_in=${DEFAULT_EXPIRES_IN}&state=${DEFAULT_ENCODED_REQUEST_STATE}`;
            oauth.checkForAuthorizationResponse();
            assert.equal(oauth.status, OAuthStatus.Unknown);
        });
        it("sets access token if there is no question mark", () => {
            config.window.location.hash = `#access_token=${DEFAULT_ACCESS_TOKEN}&expires_in=${DEFAULT_EXPIRES_IN}&state=${DEFAULT_ENCODED_REQUEST_STATE}`;
            const stubGranted = sandbox.stub(oauth, "processInboundToken");
            oauth.checkForAuthorizationResponse();
            stubGranted.calledWith(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN, DEFAULT_REQUEST_STATE);
        });
        it("sets access token if there is a no question mark and passed explicitly", () => {
            const hash = `#?access_token=${DEFAULT_ACCESS_TOKEN}&expires_in=${DEFAULT_EXPIRES_IN}&state=${DEFAULT_ENCODED_REQUEST_STATE}`;
            const stubGranted = sandbox.stub(oauth, "processInboundToken");
            oauth.checkForAuthorizationResponse(hash);
            stubGranted.calledWith(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN, DEFAULT_REQUEST_STATE);
        });
        it("does not set access token if there is an invalid expiration", (done) => {
            config.window.location.hash = `#access_token=${DEFAULT_ACCESS_TOKEN}&expires_in=ABC`;
            const stubGranted = sandbox.stub(oauth, "processInboundToken");
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.Error);
                assert.equal(data.message, 'Invalid access token expiration value: \"ABC\"');
                assert.equal(oauth.status, OAuthStatus.NotAuthenticated);
                assert(stubGranted.notCalled);
                done();
            });
            oauth.checkForAuthorizationResponse();
        });
        it("sets access token if there is no state", () => {
            config.window.location.hash = `#access_token=${DEFAULT_ACCESS_TOKEN}&expires_in=${DEFAULT_EXPIRES_IN}`;
            const stubGranted = sandbox.stub(oauth, "processInboundToken");
            oauth.checkForAuthorizationResponse();
            stubGranted.calledWith(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN);
        });
    });

    describe("processInboundToken", () => {
        it("sets access token if state is specified and dispatches notification", (done) => {
            const now = Date.now();
            sandbox.stub(Date, "now").returns(now);
            config.requestStateStorage.setItem(config.requestStateStorageKey, DEFAULT_REQUEST_STATE_WITH_NONCE);
            const stubRefreshIFrame = sandbox.stub(oauth, "setupSilentRefreshIFrame");
            oauth.subscribe((type: OAuthImplicitEventType, data: OAuthStatusInformation) => {
                assert.equal(type, OAuthImplicitEventType.TokenGranted);
                assert.equal(data.accessToken, DEFAULT_ACCESS_TOKEN);
                assert.equal(data.accessTokenExpiration, DEFAULT_EXPIRES_IN * 1000 + now);
                assert.equal(data.state, DEFAULT_REQUEST_STATE);
                assert(stubRefreshIFrame.calledOnce);
                assert.equal(oauth.status, OAuthStatus.Authenticated);
                done();
            });
            oauth.processInboundToken(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN, DEFAULT_REQUEST_STATE_WITH_NONCE);
        });
        it("sets access token if state is not specified and dispatches notification", (done) => {
            const now = Date.now();
            sandbox.stub(Date, "now").returns(now);
            config.requestStateStorage.setItem(config.requestStateStorageKey, '12345');
            const stubRefreshIFrame = sandbox.stub(oauth, "setupSilentRefreshIFrame");
            oauth.subscribe((type: OAuthImplicitEventType, data: OAuthStatusInformation) => {
                assert.equal(type, OAuthImplicitEventType.TokenGranted);
                assert.equal(data.accessToken, DEFAULT_ACCESS_TOKEN);
                assert.equal(data.accessTokenExpiration, DEFAULT_EXPIRES_IN * 1000 + now);
                assert.equal(data.state, undefined);
                assert(stubRefreshIFrame.calledOnce);
                assert.equal(oauth.status, OAuthStatus.Authenticated);
                done();
            });
            oauth.processInboundToken(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN, '12345');
        });
        it("dispatches granted if access token set and dispatches error if cannot be stored", (done) => {
            const now = Date.now();
            sandbox.stub(Date, "now").returns(now);
            const stubRefreshIFrame = sandbox.stub(oauth, "setupSilentRefreshIFrame");
            let ctr = 0;
            config.requestStateStorage.setItem(config.requestStateStorageKey, DEFAULT_REQUEST_STATE_WITH_NONCE);
            sandbox.stub(stubWindow.localStorage, "setItem")
                .throws(new Error("Error writing to local storage"));
            oauth.subscribe((type: OAuthImplicitEventType, data: OAuthStatusInformation | Error) => {
                if(type === OAuthImplicitEventType.TokenGranted) {
                    const info = <OAuthStatusInformation> data;
                    assert.equal(type, OAuthImplicitEventType.TokenGranted);
                    assert.equal(info.accessToken, DEFAULT_ACCESS_TOKEN);
                    assert.equal(info.accessTokenExpiration, DEFAULT_EXPIRES_IN * 1000 + now);
                    assert.equal(info.state, DEFAULT_REQUEST_STATE);
                    assert.equal(oauth.status, OAuthStatus.Authenticated);
                    assert(stubRefreshIFrame.calledOnce);
                    ctr++;
                } else if(type === OAuthImplicitEventType.Error) {
                    const err = <Error> data;
                    assert.equal(err.message, "Error writing to local storage");
                    ctr++;
                }
                if(ctr === 2) {
                    done();
                }
            });
            oauth.processInboundToken(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN, DEFAULT_REQUEST_STATE_WITH_NONCE);
        });

        it("dispatches error if missing stored request state", (done) => {
            const now = Date.now();
            sandbox.stub(Date, "now").returns(now);
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.Error);
                assert.equal(data.message, "Request state not found in storage");
                done();
            });
            oauth.processInboundToken(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN, DEFAULT_REQUEST_STATE_WITH_NONCE);
        });

        it("dispatches error if mismatched stored request state", (done) => {
            const now = Date.now();
            sandbox.stub(Date, "now").returns(now);
            config.requestStateStorage.setItem(config.requestStateStorageKey, DEFAULT_REQUEST_STATE_WITH_NONCE);
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.Error);
                assert.equal(data.message, "Invalid authorization state received");
                done();
            });
            oauth.processInboundToken(DEFAULT_ACCESS_TOKEN, DEFAULT_EXPIRES_IN, 'Something Else');
        });

    });

    describe("logout", () => {
        it("dispatches logout event and returns logout URL", (done) => {
            (<any> oauth)._accessToken = "ABCDEF";
            (<any> oauth)._accessTokenExpiration = 1000;
            (<any> oauth)._state = "Nee!";
            oauth.subscribe((type: OAuthImplicitEventType, data: any) => {
                assert.equal(type, OAuthImplicitEventType.Logout);
                assert.deepEqual(data, {
                    status: OAuthStatus.NotAuthenticated,
                    accessToken: undefined,
                    accessTokenExpiration: undefined,
                    state: undefined
                });
                done();
            });
            const logoutUri = oauth.logout();
            assert.equal(logoutUri, config.logoutUri);

        });
    });

    describe("subscribe", () => {
        it("adds new callback", () => {
            const testCallback = (type: OAuthImplicitEventType, data: any) => {};
            oauth.subscribe(testCallback);
            assert.deepEqual((<any>oauth)._subscriptions, [testCallback]);
        });
        it("adds new callback", () => {
            const testCallback1 = (type: OAuthImplicitEventType, data: any) => {};
            oauth.subscribe(testCallback1);
            const testCallback2 = (type: OAuthImplicitEventType, data: any) => {};
            oauth.subscribe(testCallback2);
            assert.deepEqual((<any>oauth)._subscriptions, [testCallback1, testCallback2]);
        });
        it("skips existing callback", () => {
            const testCallback = (type: OAuthImplicitEventType, data: any) => {};
            (<any>oauth)._subscriptions.push(testCallback);
            oauth.subscribe(testCallback);
            assert.deepEqual((<any>oauth)._subscriptions, [testCallback]);
        });
    });

    describe("unsubscribe", () => {
        it("removes existing callback", () => {
            const testCallback = (type: OAuthImplicitEventType, data: any) => {};
            (<any>oauth)._subscriptions.push(testCallback);
            oauth.unsubscribe(testCallback);
            assert.deepEqual((<any>oauth)._subscriptions, []);
        });
        it("skips non-existing callback", () => {
            const testCallback = (type: OAuthImplicitEventType, data: any) => {};
            oauth.unsubscribe(testCallback);
            assert.deepEqual((<any>oauth)._subscriptions, []);
        });
    });

    describe("generateNonce", () => {
        it("generates 100 unique values", () => {
            let values = {};
            for(let i = 0; i < 100; i++) {
                const nonce = oauth.generateNonce();
                let anyMatch = false;
                for(let j = 0; j < i; j++) {
                    if(values[nonce]) {
                        anyMatch = true;
                        break;
                    }
                }
                assert.equal(anyMatch, false, `Nonce value ${i+1} was not unique`);
                values[nonce] = 1;
            }
            assert.equal(Object.keys(values).length, 100);
        });
    });

    describe("setupSilentRefreshIFrame", () => {
        it("exits if there is no set access token expiration", () => {
            let timers = sandbox.useFakeTimers();
            const stubTimeout = sandbox.stub(timers, "setTimeout");
            (<any> oauth)._accessTokenExpiration = undefined;
            oauth.setupSilentRefreshIFrame();
            assert.equal(stubTimeout.called, false);
        });

        it("exits if there is no configured silentRedirectUri", () => {
            let timers = sandbox.useFakeTimers();
            const stubTimeout = sandbox.stub(timers, "setTimeout");
            (<any> oauth)._accessTokenExpiration = 100;
            config.silentRedirectUri = undefined;
            oauth.setupSilentRefreshIFrame();
            assert.equal(stubTimeout.called, false);
        });

        it("exits if there is no configured silentRefreshIFrameName", () => {
            let timers = sandbox.useFakeTimers();
            const stubTimeout = sandbox.stub(timers, "setTimeout");
            (<any> oauth)._accessTokenExpiration = 100;
            config.silentRefreshIFrameName = undefined;
            oauth.setupSilentRefreshIFrame();
            assert.equal(stubTimeout.called, false);
        });

        it("calls setTimeout with 75% of token time remaining", () => {
            stubWindow.document = {};
            let timers = sandbox.useFakeTimers();
            const now = Date.now();
            sandbox.stub(Date, "now").returns(now);
            const msAgo = 500;
            (<any> oauth)._accessTokenExpiration = (now - msAgo) + DEFAULT_EXPIRES_IN;
            const timeout = (DEFAULT_EXPIRES_IN - msAgo) * 0.75;

            const stubTimeout = sandbox.stub(timers, "setTimeout");
            oauth.setupSilentRefreshIFrame();
            assert(stubTimeout.calledWith(sinon.match.any, timeout));
        });
    });

    describe("insertSilentRefreshIFrame", () => {
        let document: HTMLDocument;
        let existingIFrame;
        let newIFrame;
        let stubGetElement: sinon.SinonStub;
        let stubRemoveChild: sinon.SinonStub;
        let stubCreateIFrame: sinon.SinonStub;
        let stubAppendChild: sinon.SinonStub;

        beforeEach(() => {
            document = (new JSDOM('<html><body></body></html>')).window.document;
            stubWindow.document = document;
            existingIFrame = {};
            newIFrame = {
                id: undefined,
                style: {
                    display: undefined,
                    width: undefined,
                    height: undefined
                },
                src: undefined,
                setAttribute: (name, value) => {
                    newIFrame[name] = value;
                }
            };
            stubGetElement = sandbox.stub(document, "getElementById");
            stubRemoveChild = sandbox.stub(document.body, "removeChild");
            stubCreateIFrame = sandbox.stub(document, "createElement").withArgs('iframe').returns(newIFrame);
            stubAppendChild = sandbox.stub(document.body, "appendChild");
        })
        
        it("inserts hidden refresh IFrame", () => {
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(existingIFrame);

            oauth.insertSilentRefreshIFrame();

            assert(newIFrame.id, config.silentRefreshIFrameName);
            assert.equal(newIFrame.src, "http://foo");
            assert.equal(newIFrame.style.display, "none");
            assert.equal(newIFrame.style.width, "1");
            assert.equal(newIFrame.style.height, "1");
            assert(stubRemoveChild.withArgs(existingIFrame).calledOnce);
            assert(stubCreateIFrame.returns(newIFrame).withArgs("iframe").calledOnce);
            assert(stubAppendChild.withArgs(newIFrame).calledOnce);
        });

        it("inserts visible refresh IFrame - no existing frame to remove", () => {
            config.displayRefreshIFrame = true;
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(undefined);

            oauth.insertSilentRefreshIFrame();

            assert(newIFrame.id, config.silentRefreshIFrameName);
            assert.equal(newIFrame.src, "http://foo");
            assert.equal(newIFrame.style.display, "block");
            assert.equal(newIFrame.style.width, "400");
            assert.equal(newIFrame.style.height, "400");
            assert(stubRemoveChild.withArgs(existingIFrame).notCalled);
            assert(stubCreateIFrame.returns(newIFrame).withArgs("iframe").calledOnce);
            assert(stubAppendChild.withArgs(newIFrame).calledOnce);
        });

        it("processes IFrame onload event with hash URL", () => {
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(existingIFrame);
            const stubProcess = sandbox.stub(oauth, "checkForAuthorizationResponse");

            oauth.insertSilentRefreshIFrame();
            newIFrame.onload({
                target: {
                    contentDocument: {
                        URL: "http://xxx#foo"
                    }
                }
            });

            assert(stubProcess.calledWith("#foo"));
        });

        it("ignores IFrame onload event with no hash URL", () => {
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(existingIFrame);
            const stubProcess = sandbox.stub(oauth, "checkForAuthorizationResponse");

            oauth.insertSilentRefreshIFrame();
            newIFrame.onload({
                target: {
                    contentDocument: {
                        URL: "http://xxx"
                    }
                }
            });

            assert(stubProcess.notCalled);
        });

        it("ignores IFrame onload event with no contentDocument URL", () => {
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(existingIFrame);
            const stubProcess = sandbox.stub(oauth, "checkForAuthorizationResponse");

            oauth.insertSilentRefreshIFrame();
            newIFrame.onload({
                target: {
                    contentDocument: {
                    }
                }
            });

            assert(stubProcess.notCalled);
        });

        it("ignores IFrame onload event with no contentDocument", () => {
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(existingIFrame);
            const stubProcess = sandbox.stub(oauth, "checkForAuthorizationResponse");

            oauth.insertSilentRefreshIFrame();
            newIFrame.onload({
                target: {
                }
            });

            assert(stubProcess.notCalled);
        });

        it("ignores IFrame onload event with no target", () => {
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(existingIFrame);
            const stubProcess = sandbox.stub(oauth, "checkForAuthorizationResponse");

            oauth.insertSilentRefreshIFrame();
            newIFrame.onload({});

            assert(stubProcess.notCalled);
        });

        it("displays onerror event", () => {
            const err = {
                message: "Nee!"
            }
            sandbox.stub(oauth, "generateAuthenticationUrl")
                .returns("http://foo")
                .withArgs(undefined, false);
            
            stubGetElement.withArgs(config.silentRefreshIFrameName).returns(existingIFrame);
            const stubConsoleError = sandbox.stub(console, "error");

            oauth.insertSilentRefreshIFrame();
            newIFrame.onerror(err);

            assert(stubConsoleError.withArgs("iframe error", err).calledOnce);
        });

    });
});