
class MockStorage {
    key: string;
    value: string;
}

export default class implements Storage {
    private _items: MockStorage[] = [];

    get length() {
        return this._items.length;
    }
    clear() {
        this._items = [];
    }
    getItem(key: string) {
        for(const item of this._items) {
            if(item.key === key) {
                return item.value;
            }
        }
        return null;
    }
    key(index: number) {
        if(index >= 0 && index <= this._items.length) {
            return this._items[index].value;
        } else {
            return null;
        }
    }
    removeItem(key: string) {
        for(let i = 0; i < this._items.length; i++) {
            if(this._items[i].key === key) {
                this._items.splice(i, 1);
            }
        }
    }
    setItem(key: string, value: string) {
        for(const item of this._items) {
            if(item.key === key) {
                item.value = value;
                return;
            }
        }
        this._items.push({
            key: key,
            value: value
        });
    }
    // [key: string] {
    //     return this.getItem(key);
    // }
}
