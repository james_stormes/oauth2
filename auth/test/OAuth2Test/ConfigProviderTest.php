<?php
/**
 * Created by PhpStorm.
 * User: jstormes
 * Date: 6/8/2018
 * Time: 3:38 PM
 */

namespace OAuth2Test;

use OAuth2\ConfigProvider;
use PHPUnit\Framework\TestCase;

class ConfigProviderTest extends TestCase
{
    public function testConfigProvider()
    {
        $config = new ConfigProvider();

        $this->assertTrue(is_array($config->__invoke()));
        $this->assertTrue(is_array($config->getDependencies()));
        $this->assertTrue(is_array($config->getTemplates()));
    }
}