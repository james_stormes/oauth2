<?php

declare(strict_types=1);

namespace OAuth2Test\Handler;

use OAuth2\Handler\SigninHandler;
use OAuth2\Handler\SigninHandlerFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Plates\PlatesRenderer;
use App\Repositories\UserRepository;
use Zend\Expressive\Router\RouterInterface;
use App\Entities\ClientEntity;


class SigninHandlerFactoryTest extends TestCase
{
    /** @var ContainerInterface */
    protected $container;

    protected function setUp()
    {
        $container = $this->prophesize(ContainerInterface::class);
        $container->get(TemplateRendererInterface::class)
            ->willReturn($this->prophesize(PlatesRenderer::class)->reveal());
        $container->get(UserRepository::class)
            ->willReturn($this->prophesize(UserRepository::class)->reveal());
        $container->get(ClientEntity::class)
            ->willReturn($this->prophesize(ClientEntity::class)->reveal());
        $container->get(RouterInterface::class)
            ->willReturn($this->prophesize(RouterInterface::class)->reveal());

        $this->container = $container->reveal();
    }

    public function testOAuth2HandlerFactory()
    {
        $factory = new SigninHandlerFactory();

        $this->assertInstanceOf(SigninHandlerFactory::class, $factory);

        $authorizationHandler = $factory($this->container);

        $this->assertInstanceOf(SigninHandler::class, $authorizationHandler);
    }

}
