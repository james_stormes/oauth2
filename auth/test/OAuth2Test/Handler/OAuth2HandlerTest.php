<?php

declare(strict_types=1);

namespace OAuth2Test\Handler;

use Grpc\Server;
use League\OAuth2\Server\Exception\OAuthServerException;
use OAuth2\Grants\GrantException;
use OAuth2\Handler\OAuth2Handler;
use OAuth2\Grants\GrantFactory;
use OAuth2\Grants\GrantAbstract;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Router\RouterInterface;

use League\OAuth2\Server\AuthorizationServer as OAuth2Server;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;

class OAuth2HandlerTest extends TestCase
{
    /** @var ContainerInterface|ObjectProphecy */
    protected $container;

    /** @var RouterInterface|ObjectProphecy */
    protected $router;


    public function testReturnsJsonResponse()
    {
        $OAuth2Server = $this->prophesize( OAuth2Server::class );
        $OAuth2Server->respondToAccessTokenRequest( Argument::any(), Argument::any() )
            ->willReturn( $this->prophesize( ResponseInterface::class)->reveal() );

        $OAuth2Handler = new OAuth2Handler($OAuth2Server->reveal());

        $this->assertInstanceOf(OAuth2Handler::class, $OAuth2Handler);

        $response = $OAuth2Handler->handle( $this->prophesize( ServerRequestInterface::class )->reveal() );

        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testCatchesOAuthServerException()
    {
        $OAuth2Server = $this->prophesize( OAuth2Server::class );
        $OAuth2Server->respondToAccessTokenRequest( Argument::any(), Argument::any() )
            ->willThrow( $this->prophesize( OAuthServerException::class)->reveal() );

        $OAuth2Handler = new OAuth2Handler($OAuth2Server->reveal());
        $response = $OAuth2Handler->handle( $this->prophesize( ServerRequestInterface::class )->reveal() );

        $body = json_decode($response->getBody()->getContents(), true);
        $this->assertArrayHasKey('oauth_exception', $body);
    }

    public function testCatchesException()
    {
        $OAuth2Server = $this->prophesize( OAuth2Server::class );
        $OAuth2Server->respondToAccessTokenRequest( Argument::any(), Argument::any() )
            ->willThrow( $this->prophesize( \Exception::class)->reveal() );

        $OAuth2Handler = new OAuth2Handler($OAuth2Server->reveal());
        $response = $OAuth2Handler->handle( $this->prophesize( ServerRequestInterface::class )->reveal() );

        $body = json_decode($response->getBody()->getContents(), true);
        $this->assertArrayHasKey('exception', $body);
    }

}
