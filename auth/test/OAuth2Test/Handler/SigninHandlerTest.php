<?php

declare(strict_types=1);

namespace OAuth2Test\Handler;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use PSR7Sessions\Storageless\Http\SessionMiddleware;
use Zend\Diactoros\Response\HtmlResponse;
use OAuth2\Handler\SigninHandler;

use App\Repositories\UserRepository;

use Zend\Expressive\Plates\PlatesRenderer;

use Prophecy\Argument;

use App\Entities\UserEntity;

use App\Entities\ClientEntity;

use Zend\Expressive\Router\RouterInterface;

use PSR7Sessions\Storageless\Session\SessionInterface;

use Zend\Diactoros\Response\RedirectResponse;



class SigninHandlerTest extends TestCase
{

    /** @var PlatesRenderer */
    protected $templateRenderer;

    /** @var UserRepository */
    protected $userRepository;

    /** @var ClientEntity */
    protected $clientEntity;

    /** @var RouterInterface */
    protected $router;


    protected function setUp()
    {
        $templateRenderer = $this->prophesize( PlatesRenderer::class );
        $templateRenderer->render( Argument::any(), Argument::any() )
            ->willReturn( "this is test" );
        $this->templateRenderer=$templateRenderer->reveal();

        $userRepository = $this->prophesize( UserRepository::class );
        $userRepository->getUserEntityByUserCredentials('gooduser','goodpassword','password', Argument::any() )
            ->willReturn($this->prophesize( UserEntity::class )->reveal());
        $userRepository->getUserEntityByUserCredentials('baduser','badpassword','password', Argument::any() )
            ->willReturn(null);
        $this->userRepository = $userRepository->reveal();

        $this->clientEntity = $this->prophesize( ClientEntity::class )->reveal();

        $router = $this->prophesize ( RouterInterface::class );
        $router->generateUri("oauth2.auth", [], [])
            ->willReturn("test");
        $this->router = $router->reveal();
    }

    public function testReturnsHtmlResponseToGet()
    {
        $page = new SigninHandler(
            $this->templateRenderer,
            $this->userRepository,
            $this->clientEntity,
            $this->router
        );

        $session = $this->prophesize(SessionInterface::class);
        $session->remove(Argument::any());

        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE)
            ->willReturn($session->reveal());
        $request->getMethod()->willReturn('GET');

        $response = $page->handle(
            $request->reveal()
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
    }

    public function testReturnsHtmlResponseToBadLoginPost()
    {
        $page = new SigninHandler(
            $this->templateRenderer,
            $this->userRepository,
            $this->clientEntity,
            $this->router
        );

        $session = $this->prophesize(SessionInterface::class);
        $session->remove(Argument::any());

        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE)
            ->willReturn($session->reveal());
        $request->getParsedBody()
            ->willReturn(['username'=>'baduser','password'=>'badpassword']);
        $request->getMethod()->willReturn('POST');

        $response = $page->handle(
            $request->reveal()
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
    }

    public function testReturnsRedirectToGoodLoginPost()
    {
        $page = new SigninHandler(
            $this->templateRenderer,
            $this->userRepository,
            $this->clientEntity,
            $this->router
        );

        $session = $this->prophesize(SessionInterface::class);
        $session->remove(Argument::any());
        $session->set(Argument::any(),Argument::any());

        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE)
            ->willReturn($session->reveal());
        $request->getParsedBody()
            ->willReturn(['username'=>'gooduser','password'=>'goodpassword']);
        $request->getQueryParams()
            ->willReturn(['a'=>'b']);
        $request->getMethod()->willReturn('POST');

        $response = $page->handle(
            $request->reveal()
        );

        $this->assertInstanceOf(RedirectResponse::class, $response);
    }


}
