<?php

declare(strict_types=1);

namespace OAuth2Test\Handler;

use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\CryptKey;
use App\Repositories\AccessTokenRepository;
use App\Repositories\ClientRepository;
use App\Repositories\ScopeRepository;
use App\Repositories\AuthCodeRepository;
use App\Repositories\RefreshTokenRepository;
use App\Repositories\UserRepository;
use OAuth2\Handler\OAuth2Handler;
use OAuth2\Handler\OAuth2HandlerFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;



class OAuth2HandlerFactoryTest extends TestCase
{
    /** @var ContainerInterface */
    protected $container;

    protected function setUp()
    {
        $container = $this->prophesize(ContainerInterface::class);

        $container->get( ClientRepository::class )
            ->willReturn($this->prophesize(ClientRepositoryInterface::class)->reveal());
        $container->get( AccessTokenRepository::class )
            ->willReturn($this->prophesize(AccessTokenRepositoryInterface::class)->reveal());
        $container->get( ScopeRepository::class )
            ->willReturn($this->prophesize(ScopeRepositoryInterface::class)->reveal());
        $container->get( AuthCodeRepository::class )
            ->willReturn($this->prophesize(AuthCodeRepositoryInterface::class)->reveal());
        $container->get( RefreshTokenRepository::class )
            ->willReturn($this->prophesize(RefreshTokenRepositoryInterface::class)->reveal());
        $container->get( UserRepository::class )
            ->willReturn($this->prophesize(UserRepositoryInterface::class)->reveal());

        $container->get( CryptKey::class )
            ->willReturn($this->prophesize(CryptKey::class)->reveal());
        $container->get( 'encryptionKey' )
            ->willReturn('lxZFUEsBCJ2Yb14IF2ygAHI5N4+ZAUXXaSeeJm6+twsUmIen');

        $this->container = $container->reveal();
    }

    public function testOAuth2HandlerFactory()
    {
        $factory = new OAuth2HandlerFactory();

        $this->assertInstanceOf(OAuth2HandlerFactory::class, $factory);

        $oauth2Handler = $factory->__invoke( $this->container );

        $this->assertInstanceOf(OAuth2Handler::class, $oauth2Handler);
    }

}
