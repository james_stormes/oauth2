<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;
use Zend\Expressive\Router\Route;

/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container) : void {
    $app->get('/', App\Handler\HomePageHandler::class, 'home');
    $app->get('/api/ping', App\Handler\PingHandler::class, 'api.ping');
    $app->get('/admin', Admin\Handler\HomePageHandler::class, 'admin');
    $app->post('/oauth2', OAuth2\Handler\OAuth2Handler::class, 'oauth2');
    $app->route('/oauth2/signin', OAuth2\Handler\SigninHandler::class, Route::HTTP_METHOD_ANY, 'oauth2.signin');
    $app->route('/oauth2/signout', OAuth2\Handler\SignoutHandler::class, Route::HTTP_METHOD_ANY, 'oauth2.signout');
    $app->route('/oauth2/authorization', OAuth2\Handler\AuthorizationHandler::class,Route::HTTP_METHOD_ANY, 'oauth2.auth');
};
