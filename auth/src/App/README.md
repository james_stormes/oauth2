
## Implementation Directory

  This is where the engineer implementing the OAuth2 server will add their specific
Repositories and Entities.  Each implementation is expected to be unique, with no
generic use case.  For example one implementation might use LDAP or AD, another 
might use a MySQL DB.  A hybrid or proxy implementation could also be implemented.

   These classes are shared between the OAuth2 and Admin code bases.  