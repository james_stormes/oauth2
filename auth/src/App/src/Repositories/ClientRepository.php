<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace App\Repositories;

use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use App\Entities\ClientEntity;

class ClientRepository implements ClientRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getClientEntity($clientIdentifier, $grantType = null, $clientSecret = null, $mustValidateSecret = true)
    {
        $clients = [
            'demoNG1.5' => [
                'secret'          => password_hash('abc123', PASSWORD_BCRYPT),
                'name'            => 'My Awesome Demo App',
                'redirect_uri'    => [
                    'https://ng15.loopback.world/#/oauth-login?a=1',
                    'https://ng15.loopback.world/silent.html',
                ],
                'is_confidential' => true,
            ],
            'demojq' => [
                'secret'          => password_hash('abc123', PASSWORD_BCRYPT),
                'name'            => 'My Awesome Demo App',
                'redirect_uri'    => [
                    'https://jquery.loopback.world/',
                ],
                'is_confidential' => true,
            ],
            'myawesomeapp' => [
                'secret'          => password_hash('abc123', PASSWORD_BCRYPT),
                'name'            => 'My Awesome Expressive App',
                'redirect_uri'    => 'https://expressive.loopback.world',
                'is_confidential' => true,
            ],
            'angularclient' => [
                'secret'          => password_hash('abc123', PASSWORD_BCRYPT),
                'name'            => 'My Awesome JS App',
                'redirect_uri'    => 'https://js.loopback.world/',
                'is_confidential' => true,
            ],
            'demo' => [
                'secret'          => password_hash('abc123', PASSWORD_BCRYPT),
                'name'            => 'My Awesome Demo App',
                'redirect_uri'    => [
                    'http://localhost/jquery/',
                    'http://localhost/ng1.5/#/oauth-login?a=1',
                    'http://localhost/ng1.5/silent.html',
                    'http://localhost/ng6/#/oauth-login?a=1',
                    'http://localhost/ng6/silent.html',
                    'http://localhost:4200/#/oauth-login?a=1',
                    'http://localhost:4200/silent.html'
                ],
                'is_confidential' => true,
            ],
        ];

        // Check if client is registered
        if (array_key_exists($clientIdentifier, $clients) === false) {
            return;
        }

        if (
            $mustValidateSecret === true
            && $clients[$clientIdentifier]['is_confidential'] === true
            && password_verify($clientSecret, $clients[$clientIdentifier]['secret']) === false
        ) {
            return;
        }

        $client = new ClientEntity();
        $client->setIdentifier($clientIdentifier);
        $client->setName($clients[$clientIdentifier]['name']);
        $client->setRedirectUri($clients[$clientIdentifier]['redirect_uri']);

        return $client;
    }
}
