<?php
/**
 * Created by PhpStorm.
 * User: jstormes
 * Date: 6/19/2018
 * Time: 11:04 AM
 */

namespace App;

use Dflydev\FigCookies\SetCookie;
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use PSR7Sessions\Storageless\Http\SessionMiddleware;

class SessionMiddlewareFactory
{
    public function __invoke() : SessionMiddleware
    {
        // the example uses a symmetric key, but asymmetric keys can also be used.
        // $privateKey = new Key('file://private_key.pem');
        // $publicKey = new Key('file://public_key.pem');
        return new SessionMiddleware(
            new Sha256(),
            'c9UA8QKLSmDEn4DhNeJIad/4JugZd/HvrjyKrS0jOes=', // signature key (important: change this to your own)
            'c9UA8QKLSmDEn4DhNeJIad/4JugZd/HvrjyKrS0jOes=', // verification key (important: change this to your own)
            SetCookie::create('state')
                ->withSecure(false) // false on purpose, unless you have https locally
                ->withHttpOnly(true)
                ->withPath('/'),
            new Parser(),
            1200, // 20 minutes
            new SystemClock()
        );
    }
}