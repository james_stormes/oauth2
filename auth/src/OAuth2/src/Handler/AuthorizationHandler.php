<?php
/**
 * Created by PhpStorm.
 * User: jstormes
 * Date: 6/6/2018
 * Time: 5:23 PM
 */

declare(strict_types=1);

namespace OAuth2\Handler;

use App\Entities\UserEntity;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use League\OAuth2\Server\AuthorizationServer as OAuth2Server;
use Zend\Expressive\Plates\PlatesRenderer;
use PSR7Sessions\Storageless\Http\SessionMiddleware;
use PSR7Sessions\Storageless\Session\DefaultSessionData;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Helper\UrlHelper;


class AuthorizationHandler implements RequestHandlerInterface
{
    /** @var PlatesRenderer  */
    private $templateEngine;

    /** @var OAuth2Server */
    private $OAuth2Server;

    /** @var UrlHelper  */
    private $urlHelper;

    /** @var DefaultSessionData */
    private $session;

    public function __construct( PlatesRenderer $templateEngine, OAuth2Server $OAuth2Server, UrlHelper $urlHelper)
    {
        $this->templateEngine = $templateEngine;
        $this->OAuth2Server = $OAuth2Server;
        $this->urlHelper = $urlHelper;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        /* @var \PSR7Sessions\Storageless\Session\DefaultSessionData $session */
        $this->session = $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);

        if ($this->session->has('user')) {

            /** @var UserEntity $user */
            $user = unserialize($this->session->get('user'));

            if ($user instanceof UserEntity){

                $authRequest = $this->OAuth2Server->validateAuthorizationRequest($request);
                $authRequest->setUser($user);

                if ($this->session->has('authorized')) {
                    $authRequest->setAuthorizationApproved(true);
                    $response = new HtmlResponse('');
                    return $this->OAuth2Server->completeAuthorizationRequest($authRequest, $response);
                }

//                if ($authRequest->isAuthorizationApproved()) {
//                    return $this->requestPreviouslyAuthorized($authRequest);
//                }

                return $this->askUserForAuthorization($request, $authRequest);
            }
        }

        return $this->redirectToSignin($request);
    }

    private function redirectToSignin($request) {
        $newUrl = $this->urlHelper->generate('oauth2.signin',[], $request->getQueryParams() );
        return new RedirectResponse( $newUrl );
    }

    private function requestPreviouslyAuthorized($authRequest) {
        $response = new HtmlResponse('');
        return $this->OAuth2Server->completeAuthorizationRequest($authRequest, $response);
    }

    private function askUserForAuthorization($request, $authRequest)
    {
        if ($request->getMethod()==="POST") {

            // If form approved
            $authRequest->setAuthorizationApproved(true);

            $this->session->set('authorized',true);

            $response = new HtmlResponse('');
            return $this->OAuth2Server->completeAuthorizationRequest($authRequest, $response);
        }

        // present Authorization request form.
        $data = [
            'scopes'=> $authRequest->getScopes()
        ];

        $authRequest->setAuthorizationApproved(false);
        return new HtmlResponse($this->templateEngine->render('authorization::default', $data));
    }

}