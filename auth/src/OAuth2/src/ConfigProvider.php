<?php

declare(strict_types=1);

namespace OAuth2;

use App\Repositories\UserRepository;
use App\Entities\ClientEntity;


/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [
                UserRepository::class => UserRepository::class,
                ClientEntity::class => ClientEntity::class
            ],
            'factories'  => [
                Handler\OAuth2Handler::class => Handler\OAuth2HandlerFactory::class,
                Handler\AuthorizationHandler::class => Handler\AuthorizationHandlerFactory::class,
                Handler\SigninHandler::class => Handler\SigninHandlerFactory::class,
                Handler\SignoutHandler::class => Handler\SignoutHandlerFactory::class,
                HtmlGateway::class => HtmlGatewayFactory::class
            ]
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'signin'    => [__DIR__ . '/../templates/signin'],
                'signout'    => [__DIR__ . '/../templates/signout'],
                'authorization'    => [__DIR__ . '/../templates/authorization']
            ],
        ];
    }
}
