var oauth = undefined;

angular.module("oauthExample", ['ngRoute', 'oauth-implicit', 'demo'])
    .config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: "templates/default.html"
            });
    });
