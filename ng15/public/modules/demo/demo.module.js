(function() {
    "use strict";

    angular.module("demo", ["ngRoute", "ngResource", "oauth-implicit"])
        .controller("demoController", ["$scope", "$resource", "oauthImplicitClientService", function($scope, $resource, oauthImplicitClientService) {
            const pingAPI = $resource("https://expressive.loopback.world/api/ping", [], {
                ping: {
                    method: "GET",
                    isArray: false,
                    headers: { 
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + oauthImplicitClientService.getAccessToken() 
                    }
                }
            });
            
            $scope.ping = function() {
                pingAPI.ping().$promise.then(
                    function(data) {
                        window.alert("Ack: " + data.ack);
                    }, function(err) {
                        window.alert(err);
                    }
                );
            };
            $scope.back = function() {
                window.location.href = '/';
            }
        }])
        .directive("demo", function() { 
            return {
                restrict: "E",
                controller: "demoController",
                templateUrl: "modules/demo/demo.html"
            };
        });
    })();