"use strict";

/**
 * Embedded Base64 encoder in case state gets complicated
 */
export const Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var c1=0,c2=0,c3=0,r=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};

/**
 * Event type enumeration
 */
export enum OAuthImplicitEventType { Initialized, TokenGranted, TokenRetrieved, TokenRenewed, TokenExpired, Logout, Error };

/**
 * Authentication status
 */
export enum OAuthStatus { Unknown, Authenticated, NotAuthenticated }

/**
 * Information to send back on event
 */
export class OAuthStatusInformation {
    readonly status: OAuthStatus;
    readonly accessToken?: string;
    readonly accessTokenExpiration?: number;
    readonly state?: string;

    constructor(status: OAuthStatus, accessToken?: string, accessTokenExpiration?: number, state?: string) {
        this.status = status;
        this.accessToken = accessToken;
        this.accessTokenExpiration = accessTokenExpiration;
        this.state = state;
    }
}

/**
 * Callback signature for OAuth observer events
 */
export type OAuthImplicitEventCallback = (type: OAuthImplicitEventType, data: Error | OAuthStatusInformation) => void;

/**
 * Configuration for OAuth Implicit workflow
 */
export class OAuthImplicitConfiguration {
    private _clientId: string;
    private _window: Window;
    private _tokenStorage: Storage;
    private _tokenStorageKey: string;
    private _requestStateStorage: Storage;
    private _requestStateStorageKey: string;
    
    constructor(clientId: string, window: Window, 
        tokenStorage: Storage = window.localStorage, tokenStorageKey: string = undefined,
        requestStateStorage: Storage = window.sessionStorage, requestStateStorageKey: string = undefined) {
        this._clientId = clientId;
        this._window = window;
        this._tokenStorage = tokenStorage;
        this._requestStateStorage = requestStateStorage;
        this._tokenStorageKey = tokenStorageKey ? tokenStorageKey : 'oauth-token-' + clientId;
        this._requestStateStorageKey = requestStateStorageKey ? requestStateStorageKey : 'oauth-request-' + clientId;
    }

    get clientId(): string {
        return this._clientId;
    }
    get window(): Window {
        return this._window;
    }
    get tokenStorage(): Storage {
        return this._tokenStorage;
    };
    get requestStateStorage(): Storage {
        return this._requestStateStorage;
    }
    get tokenStorageKey(): string {
        return this._tokenStorageKey;
    }
    get requestStateStorageKey(): string {
        return this._requestStateStorageKey;
    }

    // Other properties
    authenticationUri: string;
    redirectUri: string;
    hiddenRefreshUri: string;
    logoutUri: string;
    silentRefreshIFrameName = 'OAUTH_IFRAME';
    displayRefreshIFrame = false;
    silentRedirectUri: string;
    scopes: string;
    key: string;
}

/**
 * OAuth Implicit client handler  
 */
export class OAuthImplicit {
    private _configuration: OAuthImplicitConfiguration;
    private _subscriptions: Array<OAuthImplicitEventCallback> = [];
    private _status: OAuthStatus = OAuthStatus.Unknown;
    private _accessToken?: string = undefined;
    private _accessTokenExpiration?: number = undefined;

    constructor(configuration: OAuthImplicitConfiguration) {
        this._configuration = configuration;
    }
    
    /**
     * Return whether a valid access token exists
     */
    get status() {
        return this._status;
    };

    /**
     * Return the current access token
     */
    get accessToken() {
        return this._accessToken;
    };

    /**
     * Return the current access token expiration
     */
    get accessTokenExpiration() {
        return this._accessTokenExpiration;
    };

    /**
     * Initialize, optionally using a specific hash
     * @param hash 
     */
    initialize(hash: string = undefined) {
        // Check to see if we got an authentication response
        this.checkForAuthorizationResponse(hash);
        let status = this._status;
        if(status !== OAuthStatus.Authenticated) {
            // If this isn't a redirect with an authorization response, check storage for token
            this.checkStorageForToken();
        }
        status = this._status;
        if(status !== OAuthStatus.Authenticated) {
            // If still not authenticated, then dispatch a notification letting the client know we tried but could not authenticate
            this._status = OAuthStatus.NotAuthenticated;
            this.dispatchNotification(OAuthImplicitEventType.Initialized, new OAuthStatusInformation(OAuthStatus.NotAuthenticated));
        }
    }

    /**
     * Process a response URL, looking for access token information
     */
    checkForAuthorizationResponse(hash:string = undefined) {
        if(! hash) {
            hash = this._configuration.window.location.hash;
        } 
        hash = decodeURIComponent(hash);
        if (hash.indexOf('#') !== 0) {
            // If there is no hash, exit out
            return;
        }
        const questionMarkPosition = hash.indexOf('?');
        if (questionMarkPosition > -1) {
            hash = hash.substr(questionMarkPosition + 1);
        } else {
            hash = hash.substr(1);
        }

        let accessToken: string = undefined;
        let accessTokenExpiresIn: number = undefined;
        let state: string = undefined;

        const parts = hash.split('&');
        for(const part of parts) {
            const equalPosition = part.indexOf('=');
            if(equalPosition === -1) {
                continue;
            }
            const name = part.substr(0, equalPosition);
            const value = part.substr(equalPosition + 1);
            switch(name) {
                case 'access_token':
                    accessToken = value;
                    break;
                case 'expires_in': // seconds
                    const exp = parseInt(value);
                    if(exp) {
                        accessTokenExpiresIn = exp;
                    } else {
                        this._status = OAuthStatus.NotAuthenticated;
                        this.removeTokenStorage();
                        this.dispatchNotification(OAuthImplicitEventType.Error, new Error(`Invalid access token expiration value: "${value}"`));
                        return;
                    }
                    break;
                case 'state':
                    state = Base64.decode(value);
                    break;
            }
        }

        // If there is an access token after the hash mark, process
        if(accessToken) {
            this.processInboundToken(accessToken, accessTokenExpiresIn, state);
        }
    };

    /**
     * Check storage for access token and set as valid if not expired
     */
    checkStorageForToken() {
        try {
            const json = this._configuration.tokenStorage.getItem(this._configuration.tokenStorageKey);
            if(! json) {
                return;
            }
            const data = JSON.parse(json);
            if(! data.accessToken) {
                this.dispatchNotification(OAuthImplicitEventType.Error, new Error("accessToken not located in local storage"));
                return;
            }
            if(data.accessTokenExpiration && (Date.now() >= data.accessTokenExpiration)) {
                this.handleExpiredToken(data.accessToken, data.accessTokenExpiration);
            } else {
                this._status = OAuthStatus.Authenticated;
                this._accessToken = data.accessToken;
                this._accessTokenExpiration = data.accessTokenExpiration;
                this.setupSilentRefreshIFrame();
                this.dispatchNotification(OAuthImplicitEventType.TokenRetrieved, 
                    new OAuthStatusInformation(OAuthStatus.Authenticated, data.accessToken, data.accessTokenExpiration)
                );
            }
        } catch(e) {
            this.dispatchNotification(OAuthImplicitEventType.Error, e);
        }
    }

    /**
     * Generate a URL for authentication, generating a nonce to reduce risk of replay attack,
     * and sending state (if provided) to the server so we can get back to where we were
     */
    generateAuthenticationUrl(redirectUri: string, state: string = undefined, prompt = true) {
        const nonce = this.generateNonce();

        let stateToSend = nonce;
        if(state) {
            stateToSend += '|' + state;
        };

        // Store state in session storage so we can validate later
        this._configuration.requestStateStorage.setItem(this._configuration.requestStateStorageKey, stateToSend);

        // Generate URL
        let url = this._configuration.authenticationUri +
            '?client_id=' + this._configuration.clientId + 
            '&redirect_uri=' + encodeURIComponent(redirectUri) +
            (this._configuration.scopes ? ('&scopes=' + encodeURIComponent(this._configuration.scopes)) : '') +
            '&state=' + Base64.encode(stateToSend) +
            '&response_type=token';

        if(! prompt) {
            url += "&prompt=none";
        }
        return url;
    }
    
    /**
     * Request a token from the OAuth server, by default, storing the current URL as state
     */
    requestToken(state: string = undefined) {
        if(! state) {
            state = this._configuration.window.location.href;
        }
        this._configuration.window.location.href = this.generateAuthenticationUrl(this._configuration.redirectUri, state);
    };

    /**
     * Logout
     */
    logout() {
        this._status = OAuthStatus.NotAuthenticated;
        this._accessToken = undefined;
        this._accessTokenExpiration = undefined;
        this.dispatchNotification(OAuthImplicitEventType.Logout, new OAuthStatusInformation(OAuthStatus.NotAuthenticated));
        this.removeTokenStorage();
        return this._configuration.logoutUri;
    }

    /**
     * Subscribe callback to notifications
     * @param {*} callback 
     */
    subscribe(callback: OAuthImplicitEventCallback) {
        let add = true;
        for(const subscription of this._subscriptions) {
            if(subscription === callback) {
                add = false;
                break;
            }
        }
        if(add) {
            this._subscriptions.push(callback);
        }
    }

    /**
     * Unsubscribe callback from notifications
     * @param {*} callback 
     */
    unsubscribe(callback: OAuthImplicitEventCallback) {
        let add = false;
        this._subscriptions = this._subscriptions.filter((subscription) => {
            return subscription !== callback;
        });
    };

    /**
     * Dispatch notifications to subscribed callbacks
     */
    dispatchNotification = (event: OAuthImplicitEventType, payload: Error | OAuthStatusInformation) => {
        for(const subscription of this._subscriptions) {
            subscription(event, payload);
        }
    };

    /**
     * Generate random-ish value
     */
    generateNonce() {
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let text = '';
        for (let i = 0; i < 40; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    /**
     * Persists the token
     * @param accessToken 
     * @param accessTokenExpiresInSeconds 
     * @param state
     */
    processInboundToken(accessToken: string, accessTokenExpiresInSeconds: number, state: string): OAuthStatus {
        // Authenticate the state
        try {
            const storedState = this._configuration.requestStateStorage.getItem(this._configuration.requestStateStorageKey);
            if(! storedState) {
                throw new Error("Request state not found in storage");
            }
            if(storedState !== state) {
                throw new Error("Invalid authorization state received");
            }
        } catch(e) {
            this._status = OAuthStatus.NotAuthenticated;
            this.removeTokenStorage();
            this.dispatchNotification(OAuthImplicitEventType.Error, e);
            return;
        }
        
        if(accessTokenExpiresInSeconds < 1) {
            this.handleExpiredToken(accessToken, Date.now() + this.accessTokenExpiration);
        } else {
            this._status = OAuthStatus.Authenticated;
            this._accessToken = accessToken;
            this._accessTokenExpiration = Date.now() + accessTokenExpiresInSeconds * 1000;

            // Separate the state (if provided) from nonce
            const pipe = state.indexOf('|');
            const stateWithoutNonce = (pipe === -1) ? undefined : state.substr(pipe+1);
        
            this.setupSilentRefreshIFrame();
            this.dispatchNotification(OAuthImplicitEventType.TokenGranted, new OAuthStatusInformation(
                this._status,
                this._accessToken,
                this._accessTokenExpiration,
                stateWithoutNonce
            ));

            try {
                this._configuration.tokenStorage.setItem(this._configuration.tokenStorageKey, JSON.stringify({
                    accessToken: this._accessToken,
                    accessTokenExpiration: this._accessTokenExpiration,
                    state: stateWithoutNonce
                }));
            } catch(e) {
                this.dispatchNotification(OAuthImplicitEventType.Error, e);
            }
        }
    }
    
    /**
     * Clear token values and let subscribers know the notification expired,
     * this should get called whenever we determine that a token has expired
     */
    private handleExpiredToken(accessToken: string, accessTokenExpiration: number) {
        this._accessToken = undefined;
        this._accessTokenExpiration = undefined;
        this.dispatchNotification(OAuthImplicitEventType.TokenExpired, new OAuthStatusInformation(OAuthStatus.NotAuthenticated));
        this.removeTokenStorage();
    }

    /**
     * Remove token stored (if any)
     */
    private removeTokenStorage() {
        try {
            this._configuration.tokenStorage.removeItem(this._configuration.tokenStorageKey);
        } catch(e) {
            this.dispatchNotification(OAuthImplicitEventType.Error, e);
        }
    }

    /**
     * Create a silent refresh iFrame before the expiration, if there is an expiration and configuration for silent iFrame refresh
     */
    setupSilentRefreshIFrame() {
        if(! (this._accessTokenExpiration && this._configuration.silentRedirectUri && this._configuration.silentRefreshIFrameName && this._configuration.window.document)) {
            return;
        }
        let timeout = Math.max((this._accessTokenExpiration - Date.now()) * 0.75, 10000);
        var me = this;
        setTimeout(() => {
            me.insertSilentRefreshIFrame();
        }, timeout);
        // console.info("Refresh login set for " + timeout.toString() + " ms");
    }

    /**
     * Inserts a silent refresh iframe into the configured window's document
     */
    insertSilentRefreshIFrame() {
        const document = this._configuration.window.document;
        const existingIframe = document.getElementById(this._configuration.silentRefreshIFrameName);
        if (existingIframe) {
            document.body.removeChild(existingIframe);
        }
        // this.silentRefreshSubject = claims['sub'];
        const iframe = document.createElement('iframe');
        iframe.id = this._configuration.silentRefreshIFrameName;

        const url = this.generateAuthenticationUrl(this._configuration.silentRedirectUri, undefined, false);
        
        iframe.setAttribute('src', url);
        iframe.style['display'] = this._configuration.displayRefreshIFrame ? 'block' : 'none';
        iframe.style['width'] = this._configuration.displayRefreshIFrame ? '400' : '1';
        iframe.style['height'] = this._configuration.displayRefreshIFrame ? '400' : '1';
        iframe.onload = (e: any) => {
            // console.log("iframe loaded", e);
            if(e.target && e.target.contentDocument && e.target.contentDocument.URL) {
                const url = e.target.contentDocument.URL;
                const i = url.indexOf('#');
                if(i !== -1) {
                    this.checkForAuthorizationResponse(url.substr(i));
                }
            }
        };
        iframe.onerror = (e) => {
            console.error("iframe error", e);
        }
        document.body.appendChild(iframe);
    }
};