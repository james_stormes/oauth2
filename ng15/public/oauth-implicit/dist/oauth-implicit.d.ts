/**
 * Embedded Base64 encoder in case state gets complicated
 */
export declare const Base64: {
    _keyStr: string;
    encode: (e: any) => string;
    decode: (e: any) => string;
    _utf8_encode: (e: any) => string;
    _utf8_decode: (e: any) => string;
};
/**
 * Event type enumeration
 */
export declare enum OAuthImplicitEventType {
    Initialized = 0,
    TokenGranted = 1,
    TokenRetrieved = 2,
    TokenRenewed = 3,
    TokenExpired = 4,
    Logout = 5,
    Error = 6
}
/**
 * Authentication status
 */
export declare enum OAuthStatus {
    Unknown = 0,
    Authenticated = 1,
    NotAuthenticated = 2
}
/**
 * Information to send back on event
 */
export declare class OAuthStatusInformation {
    readonly status: OAuthStatus;
    readonly accessToken?: string;
    readonly accessTokenExpiration?: number;
    readonly state?: string;
    constructor(status: OAuthStatus, accessToken?: string, accessTokenExpiration?: number, state?: string);
}
/**
 * Callback signature for OAuth observer events
 */
export declare type OAuthImplicitEventCallback = (type: OAuthImplicitEventType, data: Error | OAuthStatusInformation) => void;
/**
 * Configuration for OAuth Implicit workflow
 */
export declare class OAuthImplicitConfiguration {
    private _clientId;
    private _window;
    private _tokenStorage;
    private _tokenStorageKey;
    private _requestStateStorage;
    private _requestStateStorageKey;
    constructor(clientId: string, window: Window, tokenStorage?: Storage, tokenStorageKey?: string, requestStateStorage?: Storage, requestStateStorageKey?: string);
    readonly clientId: string;
    readonly window: Window;
    readonly tokenStorage: Storage;
    readonly requestStateStorage: Storage;
    readonly tokenStorageKey: string;
    readonly requestStateStorageKey: string;
    authenticationUri: string;
    redirectUri: string;
    hiddenRefreshUri: string;
    logoutUri: string;
    silentRefreshIFrameName: string;
    displayRefreshIFrame: boolean;
    silentRedirectUri: string;
    scopes: string;
    key: string;
}
/**
 * OAuth Implicit client handler
 */
export declare class OAuthImplicit {
    private _configuration;
    private _subscriptions;
    private _status;
    private _accessToken?;
    private _accessTokenExpiration?;
    constructor(configuration: OAuthImplicitConfiguration);
    /**
     * Return whether a valid access token exists
     */
    readonly status: OAuthStatus;
    /**
     * Return the current access token
     */
    readonly accessToken: string;
    /**
     * Return the current access token expiration
     */
    readonly accessTokenExpiration: number;
    /**
     * Initialize, optionally using a specific hash
     * @param hash
     */
    initialize(hash?: string): void;
    /**
     * Process a response URL, looking for access token information
     */
    checkForAuthorizationResponse(hash?: string): void;
    /**
     * Check storage for access token and set as valid if not expired
     */
    checkStorageForToken(): void;
    /**
     * Generate a URL for authentication, generating a nonce to reduce risk of replay attack,
     * and sending state (if provided) to the server so we can get back to where we were
     */
    generateAuthenticationUrl(redirectUri: string, state?: string, prompt?: boolean): string;
    /**
     * Request a token from the OAuth server, by default, storing the current URL as state
     */
    requestToken(state?: string): void;
    /**
     * Logout
     */
    logout(): string;
    /**
     * Subscribe callback to notifications
     * @param {*} callback
     */
    subscribe(callback: OAuthImplicitEventCallback): void;
    /**
     * Unsubscribe callback from notifications
     * @param {*} callback
     */
    unsubscribe(callback: OAuthImplicitEventCallback): void;
    /**
     * Dispatch notifications to subscribed callbacks
     */
    dispatchNotification: (event: OAuthImplicitEventType, payload: Error | OAuthStatusInformation) => void;
    /**
     * Generate random-ish value
     */
    generateNonce(): string;
    /**
     * Persists the token
     * @param accessToken
     * @param accessTokenExpiresInSeconds
     * @param state
     */
    processInboundToken(accessToken: string, accessTokenExpiresInSeconds: number, state: string): OAuthStatus;
    /**
     * Clear token values and let subscribers know the notification expired,
     * this should get called whenever we determine that a token has expired
     */
    private handleExpiredToken;
    /**
     * Remove token stored (if any)
     */
    private removeTokenStorage;
    /**
     * Create a silent refresh iFrame before the expiration, if there is an expiration and configuration for silent iFrame refresh
     */
    setupSilentRefreshIFrame(): void;
    /**
     * Inserts a silent refresh iframe into the configured window's document
     */
    insertSilentRefreshIFrame(): void;
}
