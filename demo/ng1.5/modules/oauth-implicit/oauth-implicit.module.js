(function() {
    "use strict";

    /****
     * To get the Angular 1.x router working with the League"s OAuth server, we have to create a page,
     * with an appended query string, so that the OAuth server will inject the credentials to the query string
     * without adding an additional hash mark.  This will probably break if Html5Mode is enabled
     ****/
    const OAUTH_REDIRECT_URL = "oauth-login?a=1";

    angular.module("oauth-implicit", ["ngRoute"])

    /**
     * This service loads the OAuth implicit class, and handles observer notifications
     * on state changes
     */
    .service("oauthImplicitClientService", ["$rootScope", "$location", function ($rootScope, $location) {
        let oauth = undefined;
        let queuedHash = undefined;
        SystemJS.import("../../../oauth-implicit/dist/oauth-implicit.js").then((OAuthNamespace) => {
            var config = new OAuthNamespace.OAuthImplicitConfiguration("demo", window);
            config.authenticationUri = "https://auth.loopback.world/oauth2/authorization";
            config.redirectUri = window.location.href + OAUTH_REDIRECT_URL;
            config.silentRedirectUri = $location.absUrl().replace(/\#\//g, "silent.html");
            config.silentRefreshIFrameName = "silent-refresh";

            oauth = new OAuthNamespace.OAuthImplicit(config);
            oauth.subscribe((type, data) => {
                console.log("Received OAuth Notification: " + type.toString());
                console.log(data);
                if(type === OAuthNamespace.OAuthImplicitEventType.Error) {
                    $rootScope.$broadcast("error", data);
                    // On error, switch UI back to not auth state
                    window.alert(data.message);
                } else {
                    if(data.status === OAuthNamespace.OAuthStatus.Authenticated) {
                        $rootScope.$broadcast("authenticated", data);
                    } else if (data.status === OAuthNamespace.OAuthStatus.NotAuthenticated) {
                        $rootScope.$broadcast("not-authenticated", data);
                    }
                    if(type === OAuthNamespace.OAuthImplicitEventType.TokenGranted) {
                        // If the token was granted, clear out any hash and redirect to the state, which was our original URL
                        history.pushState("", document.title, window.location.pathname);
                        if(data.state) {
                            window.location.href = data.state;
                        }
                    }
                }
            });

            oauth.initialize(queuedHash);
        });

        // Published service methods
        return {
            login: function() {
                oauth.requestToken();
            },
            logout: function() {
                oauth.logout();
            },
            checkForAuthorizationResponse: function(hash) {
                // If we haven't loaded up the oauth implicit service yet, stash the hash
                if(oauth) {
                    oauth.checkForAuthorizationResponse(hash);
                } else {
                    queuedHash = hash;
                }
            },
            getStatus: function() {
                return oauth ? oauth.status : null;
            },
            getAccessToken: function() {
                return oauth ? oauth.accessToken : null;
            }
        };
    }])

    /**
     * Injector to include bearer token in headers
     */
    .factory("oauthResponseInterceptor", ["$q", "oauthImplicitClientService", function($q, oauthImplicitClientService) {
        return {
            request: function (config)  
            {  
                if (oauthImplicitClientService) {
                    if(oauthImplicitClientService.getStatus() === 1) {
                        config.headers["Authorization"] = "Bearer " + oauthImplicitClientService.getAccessToken();
                    }
                }
                return config;  
            },  
        };
    }])

    /**
     * Directive for a "authenticated" attribute
     * set to "true" to show contents if authenticated
     * set to "false" to show contents if not authenticated
     * set to "force" to trigger an OAuth token request if not authenticated
     */
    .directive("authenticated", function() { 
        return {
            restrict: "A",
            controller: "authenticatedController",
            scope: {
                authenticated: "@authenticated"
            },
            compile: function (element) {
                element.addClass("ng-hide");
            }
        };
    })

    /**
     * Controller for the "authenticated" attribute
     */
    .controller("authenticatedController", ["$scope", "$element", "$animate", "oauthImplicitClientService",
        function($scope, $element, $animate, oauthImplicitClientService) {
            function isTruthy(value) {
                switch(value.toLowerCase()) {
                    case "true":
                    case "yes":
                    case "1":
                        return true;
                    default:
                        return false;
                }
            }
        
            const force = $scope.authenticated === "force";
            const showIfAuthenticated = isTruthy($scope.authenticated) || force;
            const showIfNotAuthenticated = ! showIfAuthenticated;

            function updateElement(authenticated) {
                if(showIfAuthenticated) {
                    if(authenticated) {
                        $element.removeClass("ng-hide").removeAttr("hidden");
                    } else {
                        $element.addClass("ng-hide").attr("hidden", "");
                    }
                } else if (showIfNotAuthenticated) {
                    if(authenticated) {
                        $element.addClass("ng-hide").attr("hidden", "");
                    } else {
                        $element.removeClass("ng-hide").removeAttr("hidden");
                    }
                }
            }

            /**
             * Check the status of authentication, recursively check again
             * if the status is not yet determined or if service isn't fully stood up yet
             */
            function checkStatus() {
                if(oauthImplicitClientService) {
                    switch(oauthImplicitClientService.getStatus()) {
                        case 1:
                            updateElement(true);
                            break;
                        case 2:
                            // If we are forcing authentication, and have not authenticated yet, force it
                            if(force) {
                                oauthImplicitClientService.login();
                            } else {
                                updateElement(false);
                            }
                            break;
                        default:
                            setTimeout(() => { checkStatus(); }, 100);
                            break;
                    }
                } else {
                    setTimeout(() => { checkStatus(); }, 100);
                }
            }

            /**
             * Respond to events from our implicit authentication service
             */
            $scope.$on("authenticated", function(event, data) {
                updateElement(true);
            });
            $scope.$on("not-authenticated", function(event, data) {
                updateElement(false);
            });

            checkStatus();
        }
    ])

    /**
     * Controller for Authentication actions
     */
    .controller("authentication", ["$scope", "oauthImplicitClientService",
        function($scope, oauthImplicitClientService) {
            $scope.login = function() {
                oauthImplicitClientService.login();
            };
            $scope.logout = function() {
                oauthImplicitClientService.logout();
            }
        }
    ])

    /**
     * Controller to capture inbound authentication info from OAuth
     */
    .controller("processAuthenticationController", ["$scope", "$location", "oauthImplicitClientService",
        function($scope, $location, oauthImplicitClientService) {
            oauthImplicitClientService.checkForAuthorizationResponse("#" + $location.$$url);
        }
    ])

    /**
     * Module configuration
     */
    .config(["$httpProvider", "$routeProvider", function($httpProvider, $routeProvider) {
        $httpProvider.interceptors.push("oauthResponseInterceptor");
        $routeProvider.when("/oauthLogin", {
            controller: "processAuthenticationController",
            template: ""
        })
    }]);
    
})();