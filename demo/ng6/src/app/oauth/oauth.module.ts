import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OAuthRoutingModule } from './oauth-routing.module';
import { OAuthLoginComponent } from './components/oauth-login/oauth-login.component';
import { AuthenticatedDirective } from './directives/authenticated.directive';

@NgModule({
    imports: [
        CommonModule,
        OAuthRoutingModule
    ],
    exports: [
        AuthenticatedDirective
    ],
    declarations: [
        OAuthLoginComponent,
        AuthenticatedDirective
    ]
})
export class OAuthModule { }
