import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OAuthLoginComponent } from './components/oauth-login/oauth-login.component';

const routes: Routes = [
    {
        path: 'oauth-login',
        component: OAuthLoginComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OAuthRoutingModule { }
