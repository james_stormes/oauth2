import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OAuthImplicitEventType, OAuthStatus, OAuthStatusInformation, OAuthImplicitConfiguration, OAuthImplicit } from '../../../../../oauth-implicit/src/oauth-implicit';
import { environment } from '../../../environments/environment';

export class OAuthImplicitNotification {
    type: OAuthImplicitEventType;
    data: OAuthStatusInformation | Error;
}

@Injectable({
    providedIn: 'root'
})
export class OAuthImplicitService extends EventEmitter<OAuthImplicitNotification> {
    private oauth: OAuthImplicit;

    constructor(private HttpClient: HttpClient) {
        super();
        const config = new OAuthImplicitConfiguration("demo", window);
        config.authenticationUri = environment.oauth.authenticationUri;
        config.redirectUri = environment.oauth.redirectUri;
        config.silentRedirectUri = environment.oauth.silentRedirectUri;
        config.silentRefreshIFrameName = "silent-refresh";

        this.oauth = new OAuthImplicit(config);
        this.oauth.subscribe((type, data) => {
            console.log("Received OAuth Notification: " + type.toString());
            console.log(data);
            this.emit({
                type: type,
                data: data
            });
            if (type === OAuthImplicitEventType.TokenGranted) {
                // If the token was granted, clear out any hash and redirect to the state, which was our original URL
                history.pushState("", document.title, window.location.pathname);
                const info = <OAuthStatusInformation> data;
                if (info.state) {
                    if(window.location.href !== info.state) {
                        window.location.href = info.state;
                    }
                }
            }
        });
        this.oauth.initialize();
    }

    login() {
        this.oauth.requestToken();
    }

    logout() {
        this.oauth.logout();
    }

    checkForAuthorizationResponse(hash: string) {
        this.oauth.checkForAuthorizationResponse(hash);
    }

    get status() {
        return this.oauth.status;
    }

    get accessToken() {
        return this.oauth.accessToken;
    }
}
