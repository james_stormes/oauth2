import { TestBed, inject } from '@angular/core/testing';

import { OAuthImplicitService } from './oauth-implicit.service';

describe('OauthImplicitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OAuthImplicitService]
    });
  });

  it('should be created', inject([OAuthImplicitService], (service: OAuthImplicitService) => {
    expect(service).toBeTruthy();
  }));
});
