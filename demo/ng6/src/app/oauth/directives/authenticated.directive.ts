import { Directive, ElementRef, OnInit } from '@angular/core';
import { OAuthImplicitService } from './../services/oauth-implicit.service';
import { OAuthImplicitEventType, OAuthStatus, OAuthStatusInformation, OAuthImplicitConfiguration, OAuthImplicit } from '../../../../../oauth-implicit/src/oauth-implicit';

@Directive({
    selector: '[authenticated]'
})
export class AuthenticatedDirective implements OnInit {
    private element: HTMLInputElement;
    private defaultDisplay: string;
    private showIfAuthenticated = false;
    private showIfNotAuthenticated = false;

    constructor(private elementRef: ElementRef, private oauth: OAuthImplicitService) {
        this.element = elementRef.nativeElement;
    }

    isTruthy(value: string) {
        switch (value.toLowerCase()) {
            case "true":
            case "yes":
            case "1:":
                return true;
            default:
                return false;
        }
    }

    ngOnInit() {
        const attr = this.element.attributes.getNamedItem('authenticated');
        if (attr) {
            this.showIfAuthenticated = this.isTruthy(attr.value) || attr.value === "force";
            this.showIfNotAuthenticated = !this.showIfAuthenticated
            this.oauth.subscribe((type: OAuthImplicitEventType, data: OAuthStatusInformation | Error) => {
                if(type === OAuthImplicitEventType.Error) {
                    window.alert((<Error> data).message);
                }
                this.update();
            });
            this.defaultDisplay = this.element.style.display;
            this.element.style.display = "none";
            this.update();
        }
    }

    update() {
        switch (this.oauth.status) {
            case OAuthStatus.Authenticated:
                if (this.showIfAuthenticated) {
                    this.element.style.display = this.defaultDisplay;
                } else if (this.showIfNotAuthenticated) {
                    this.element.style.display = "none";
                }
                break;
            case OAuthStatus.NotAuthenticated:
                if (this.showIfAuthenticated) {
                    this.element.style.display = "none";
                } else if (this.showIfNotAuthenticated) {
                    this.element.style.display = this.defaultDisplay;
                }
                break;
        }

    }
}
