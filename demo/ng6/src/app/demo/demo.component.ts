import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OAuthImplicitService } from '../oauth/services/oauth-implicit.service';
import { environment } from '../../environments/environment';

@Component({
    selector: 'app-demo',
    templateUrl: './demo.component.html',
    styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

    constructor(private oauth: OAuthImplicitService, private http: HttpClient) {
    }

    ngOnInit() {
    }

    login() {
        this.oauth.login();
    }

    logout() {
        this.oauth.logout();
    }

    ping() {
        this.http.get(environment.pingUri, {
            headers: new HttpHeaders({
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.oauth.accessToken}`
            })
        }).subscribe(
            (data: any) => { window.alert(`ack: ${data.ack}`); },
            (err) => window.alert(`Error: ${err}`)
        );
    }

    back() {
        window.location.href = '/';
    }

}
