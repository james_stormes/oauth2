import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http'; 
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';


import { OAuthModule } from './oauth/oauth.module';

@NgModule({
    declarations: [
        AppComponent,
        DemoComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        HttpClientModule,
        OAuthModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
