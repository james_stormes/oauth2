export const environment = {
    production: true,
    oauth: {
        authenticationUri: "https://auth.loopback.world/oauth2/authorization",
        redirectUri: "http://localhost/ng6/#/oauth-login?a=1",
        silentRedirectUri: "http://localhost/ng6/silent.html"
    },
    pingUri: "https://expressive.loopback.world/api/ping"
};
