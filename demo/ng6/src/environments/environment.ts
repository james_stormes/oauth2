// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    oauth: {
        authenticationUri: "https://auth.loopback.world/oauth2/authorization",
        redirectUri: "http://localhost:4200/#/oauth-login?a=1",
        silentRedirectUri: "http://localhost:4200/silent.html"
    },
    pingUri: "https://expressive.loopback.world/api/ping"
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
