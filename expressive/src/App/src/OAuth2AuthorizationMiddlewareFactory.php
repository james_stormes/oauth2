<?php
/**
 * Created by PhpStorm.
 * User: jstormes
 * Date: 6/22/2018
 * Time: 2:58 PM
 */

namespace App;


use OAuth2\Crypt\CryptKey;
use League\OAuth2\Client\Provider\GenericProvider;
use OAuth2\Middleware\OAuth2AuthorizationMiddleware;
use OAuth2\Entities\OAuth2Entity;

class OAuth2AuthorizationMiddlewareFactory
{
    public function __invoke() : OAuth2AuthorizationMiddleware
    {
        $options = [
            'clientId'                => 'myawesomeapp',    // The client ID assigned to you by the provider
            'clientSecret'            => 'abc123',   // The client password assigned to you by the provider
            'redirectUri'             => 'https://expressive.loopback.world',
            'urlAuthorize'            => 'https://auth.loopback.world/oauth2/authorization',
            'urlAccessToken'          => 'https://auth.loopback.world/oauth2',
            'urlResourceOwnerDetails' => 'https://auth.loopback.world/oauth2/user/resource'
        ];

        $provider = new GenericProvider($options);

        $OAuth2Prototype = new OAuth2Entity();

        $cryptKey = new CryptKey("/var/www/auth/public.key", null, false);
        $OAuth2Prototype->setKey($cryptKey);

        return new OAuth2AuthorizationMiddleware(
            $OAuth2Prototype,
            $provider
        );
    }
}