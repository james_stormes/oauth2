<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: jstormes
 * Date: 8/15/2018
 * Time: 11:35 AM
 */

namespace App;

use Tuupola\Middleware\CorsMiddleware;
use Zend\Diactoros\Response;
use Zend\Stratigility\Middleware\CallableMiddlewareWrapper;

class CorsMiddlewareFactory
{
    public function __invoke($container)
    {
        return new CorsMiddleware([
            "origin" => ["*"],
            "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"],
            "headers.allow" => ["Content-Type", "Accept", "Authorization"],
            "headers.expose" => [],
            "credentials" => false,
            "cache" => 0,
        ]);

    }
}