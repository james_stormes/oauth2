<?php

declare(strict_types=1);

namespace App\Handler;

use League\OAuth2\Client\Provider\GenericProvider;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

class HomePageHandler implements RequestHandlerInterface
{
    private $containerName;

    private $router;

    private $template;

    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        string $containerName
    ) {
        $this->router        = $router;
        $this->template      = $template;
        $this->containerName = $containerName;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
//        $provider = new GenericProvider([
//            'clientId'                => 'myawesomeapp',    // The client ID assigned to you by the provider
//            'clientSecret'            => 'abc123',   // The client password assigned to you by the provider
//            'redirectUri'             => 'https://expressive.loopback.world',
//            'urlAuthorize'            => 'https://auth.loopback.world/oauth2/authorization',
//            'urlAccessToken'          => 'https://auth.loopback.world/oauth2',
//            'urlResourceOwnerDetails' => 'http://brentertainment.com/oauth2/lockdin/resource'
//        ]);
//
//        // If we don't have an authorization code then get one
//        if (!isset($_GET['code'])) {
//
//            // Fetch the authorization URL from the provider; this returns the
//            // urlAuthorize option and generates and applies any necessary parameters
//            // (e.g. state).
//            $authorizationUrl = $provider->getAuthorizationUrl();
//
//            // Get the state generated for you and store it to the session.
//            $_SESSION['oauth2state'] = $provider->getState();
//
//            // Redirect the user to the authorization URL.
//            header('Location: ' . $authorizationUrl);
//            exit;
//
//        // Check given state against previously stored one to mitigate CSRF attack
//        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {
//
//            if (isset($_SESSION['oauth2state'])) {
//                unset($_SESSION['oauth2state']);
//            }
//
//            exit('Invalid state');
//
//        } else {
//
//            try {
//
//                // Try to get an access token using the authorization code grant.
//                $accessToken = $provider->getAccessToken('authorization_code', [
//                    'code' => $_GET['code']
//                ]);
//
//                // We have an access token, which we may use in authenticated
//                // requests against the service provider's API.
//                echo 'Access Token: ' . $accessToken->getToken() . "<br>";
//                echo 'Refresh Token: ' . $accessToken->getRefreshToken() . "<br>";
//                echo 'Expired in: ' . $accessToken->getExpires() . "<br>";
//                echo 'Already expired? ' . ($accessToken->hasExpired() ? 'expired' : 'not expired') . "<br>";
//
//                // Using the access token, we may look up details about the
//                // resource owner.
//                $resourceOwner = $provider->getResourceOwner($accessToken);
//
//                var_export($resourceOwner->toArray());
//
//                // The provider provides a way to get an authenticated API request for
//                // the service, using the access token; it returns an object conforming
//                // to Psr\Http\Message\RequestInterface.
//                $request = $provider->getAuthenticatedRequest(
//                    'GET',
//                    'http://brentertainment.com/oauth2/lockdin/resource',
//                    $accessToken
//                );
//
//            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
//
//                // Failed to get the access token or user details.
//                exit($e->getMessage());
//
//            }
//
//        }




        if (! $this->template) {
            return new JsonResponse([
                'welcome' => 'Congratulations! You have installed the zend-expressive skeleton application.',
                'docsUrl' => 'https://docs.zendframework.com/zend-expressive/',
            ]);
        }

        $data = [];

        switch ($this->containerName) {
            case 'Aura\Di\Container':
                $data['containerName'] = 'Aura.Di';
                $data['containerDocs'] = 'http://auraphp.com/packages/2.x/Di.html';
                break;
            case 'Pimple\Container':
                $data['containerName'] = 'Pimple';
                $data['containerDocs'] = 'https://pimple.symfony.com/';
                break;
            case 'Zend\ServiceManager\ServiceManager':
                $data['containerName'] = 'Zend Servicemanager';
                $data['containerDocs'] = 'https://docs.zendframework.com/zend-servicemanager/';
                break;
            case 'Auryn\Injector':
                $data['containerName'] = 'Auryn';
                $data['containerDocs'] = 'https://github.com/rdlowrey/Auryn';
                break;
            case 'Symfony\Component\DependencyInjection\ContainerBuilder':
                $data['containerName'] = 'Symfony DI Container';
                $data['containerDocs'] = 'https://symfony.com/doc/current/service_container.html';
                break;
        }

        if ($this->router instanceof Router\AuraRouter) {
            $data['routerName'] = 'Aura.Router';
            $data['routerDocs'] = 'http://auraphp.com/packages/2.x/Router.html';
        } elseif ($this->router instanceof Router\FastRouteRouter) {
            $data['routerName'] = 'FastRoute';
            $data['routerDocs'] = 'https://github.com/nikic/FastRoute';
        } elseif ($this->router instanceof Router\ZendRouter) {
            $data['routerName'] = 'Zend Router';
            $data['routerDocs'] = 'https://docs.zendframework.com/zend-router/';
        }

        if ($this->template instanceof PlatesRenderer) {
            $data['templateName'] = 'Plates';
            $data['templateDocs'] = 'http://platesphp.com/';
        } elseif ($this->template instanceof TwigRenderer) {
            $data['templateName'] = 'Twig';
            $data['templateDocs'] = 'http://twig.sensiolabs.org/documentation';
        } elseif ($this->template instanceof ZendViewRenderer) {
            $data['templateName'] = 'Zend View';
            $data['templateDocs'] = 'https://docs.zendframework.com/zend-view/';
        }

        return new HtmlResponse($this->template->render('app::home-page', $data));
    }
}
