<?php
/**
 * Created by PhpStorm.
 * User: jstormes
 * Date: 8/2/2018
 * Time: 3:54 PM
 */

namespace  OAuth2\Entities;

use OAuth2\Entities\OAuth2EntityAbstract;

use OAuth2\Entities\CryptKeyTrait;

class OAuth2Entity extends OAuth2EntityAbstract
{
    use CryptKeyTrait;
}
